// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.ArmConstants;
import frc.robot.Constants.CAN;
import frc.robot.Constants.SolenoidChannel;
import frc.robot.components.PressureDetector;
import frc.robot.networktables.ArmTable;
import static frc.robot.Constants.ArmConstants.*;


public class ShoulderSubsystem extends SubsystemBase {

  private DoubleSolenoid pivotSolenoid;

  private ArmTable table;

  private double timeWhenFinished;

  /** Creates a new ArmShoulderSubsystem. */
  public ShoulderSubsystem(PressureDetector robotPressureDetector) {
  pivotSolenoid = new DoubleSolenoid(CAN.kPneumaticHub, PneumaticsModuleType.REVPH, 
  SolenoidChannel.kArmPivotForward,
  SolenoidChannel.kArmPivotReverse);
  // initial position
  pivotSolenoid.set(Value.kReverse);

  timeWhenFinished = Timer.getFPGATimestamp();

  table = new ArmTable();
  }
  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    table.setShoulderExtended(pivotSolenoid.get() == Value.kForward);
  }

  /**
   * Sets the shoulder to be upright by retracting the piston.
   */
  public void retractShoulder() {
    if(!isShoulderIn()) {
      timeWhenFinished = Timer.getFPGATimestamp() + ArmConstants.kArmDeployTime;
    }

    pivotSolenoid.set(Value.kReverse);
  }

  /**
   * Sets the shoulder to be tilted out by extending the piston.
   */
  public void deployShoulder() {
    if(!isShoulderOut()) {
      timeWhenFinished = Timer.getFPGATimestamp() + ArmConstants.kArmDeployTime;
    }

    pivotSolenoid.set(Value.kForward);
  }

  /**
   * Determines whether the shoulder is in. If the shoulder is in motion,
   * or there is not enough pressure the position is ambiguous.
   * 
   * If you need the shoulder to be out, you should use {@link ArmSubsystem#isShoulderOut()}.
   * 
   * @return true if the shoulder is upright; false otherwise.
   */
  public boolean isShoulderIn() {
    if(Timer.getFPGATimestamp() < timeWhenFinished) {
      return false;
    }
    
    return pivotSolenoid.get() == Value.kReverse;
  }

  /**
   * Determines whether the shoulder is out. If the shoulder is in motion,
   * or there is not enough pressure the position is ambiguous.
   * 
   * If you need the shoulder to be in, you should use {@link ArmSubsystem#isShoulderIn()}.
   * 
   * @return true if the shoulder is tilted forward; false otherwise.
   */
  public boolean isShoulderOut() {
    if(Timer.getFPGATimestamp() < timeWhenFinished) {
      return false;
    }

    return pivotSolenoid.get() == Value.kForward;
  }

  /**
   * Calculates the arms position based on its measurements.
   * If the arm is in motion an estimated position is determined
   * 
   * @return the upper arm position in degrees
   */
  public double getUpperArmPosition() {

    if (isShoulderIn()) {
      return kUpperArmInAngle;
    }
    if (isShoulderOut()) {
      return kUpperArmOutAngle;
    }

    // interpolate the values
    double percentLeft = MathUtil.clamp(
      (timeWhenFinished - Timer.getFPGATimestamp()) / ArmConstants.kArmDeployTime,
       0, 1);

    if (pivotSolenoid.get() == Value.kForward) {
      return MathUtil.interpolate(kUpperArmOutAngle, kUpperArmInAngle, percentLeft);
    }
    else if (pivotSolenoid.get() == Value.kReverse) {
      return MathUtil.interpolate(kUpperArmInAngle, kUpperArmOutAngle, percentLeft);
    }

    // assume the arm is in
    return kUpperArmInAngle;
  }
}