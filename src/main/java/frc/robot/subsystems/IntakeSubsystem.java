// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.RobotBase;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import frc.robot.Constants.CAN;
import frc.robot.Constants.IntakeConstants;
import frc.robot.Constants.SolenoidChannel;
import frc.robot.components.PressureDetector;
import frc.robot.networktables.IntakeTable;
import frc.robot.simulation.simsystems.IntakeSim;

public class IntakeSubsystem extends SubsystemBase {

  private DoubleSolenoid intakeSolenoid;

  private CANSparkMax intakeMotorUpper;
  private CANSparkMax intakeMotorLower;

  private IntakeTable intakeTable;

  private PressureDetector pressureDetector;

  private IntakeSim sim;
  private double timeWhenFinished;

  /** Creates a new IntakeSubsystem. */
  public IntakeSubsystem(PressureDetector robotPressureDetector) {
    intakeSolenoid = new DoubleSolenoid(CAN.kPneumaticHub, PneumaticsModuleType.REVPH, 
      SolenoidChannel.kIntakeForward, 
      SolenoidChannel.kIntakeReverse);
    
    intakeSolenoid.set(Value.kReverse);

    intakeMotorUpper = new CANSparkMax(CAN.kIntakeMotorUpper, MotorType.kBrushless);
    intakeMotorLower = new CANSparkMax(CAN.kIntakeMotorLower, MotorType.kBrushless);

    timeWhenFinished = Timer.getFPGATimestamp();

    intakeTable = new IntakeTable(); 
    pressureDetector = robotPressureDetector;

    if (RobotBase.isSimulation()) {
      sim = new IntakeSim();
    }
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    
    intakeTable.setPressure(pressureDetector.getSensorValue());
    intakeTable.setIntakePosition(isDeployed());
  }

  /**
   * Uses the solenoid to deploy the intake out of the robot
   */
  public void deployIntake() {
    if(!isDeployed()) {
      timeWhenFinished = Timer.getFPGATimestamp() + IntakeConstants.kIntakeDeployTime;
    }
  
    if(RobotBase.isSimulation()) {
      sim.setIntakeSolenoidForward();
    }

    intakeSolenoid.set(Value.kForward);
  }

  /**
   * Uses the solenoid to retract the intake into the robot
   */
  public void retractIntake() {
    if(!isRetracted()) {
      timeWhenFinished = Timer.getFPGATimestamp() + IntakeConstants.kIntakeDeployTime;
    }
    
    if(RobotBase.isSimulation()) {
      sim.setIntakeSolenoidReverse();
    }

    intakeSolenoid.set(Value.kReverse);
  }

  /**
   * 
   * @return whether the intake is deployed or retracted
   */
  public boolean isDeployed() {
    if(Timer.getFPGATimestamp() < timeWhenFinished) {
      return false;
    }
    
    // simulation
    if(RobotBase.isSimulation()) {
      return sim.getIntakeSolenoid() == Value.kForward;
    }

    return intakeSolenoid.get() == Value.kForward;
  }

  public boolean isRetracted() {
    if(Timer.getFPGATimestamp() < timeWhenFinished) {
      return false;
    }
    
    if(RobotBase.isSimulation()) {
      return sim.getIntakeSolenoid() == Value.kReverse;
    }

    return intakeSolenoid.get() == Value.kReverse;
  }

  /**
   * Uses the motor to spin the wheels to suck in cones and cubes
   * @param power power is a number between -1 and 1 with 1 being max intake
   */
  // public void blackHole(double power) {
  //   intakeMotorUpper.set(power);
  //   intakeMotorLower.set(power);
  // }

  public void blackHole(double upperPower, double lowerPower) {
    intakeMotorUpper.set(upperPower);
    intakeMotorLower.set(lowerPower);
  }

  /**
   * Stop motor
   */
  public void stopIntake() {
    intakeMotorUpper.stopMotor();
    intakeMotorLower.stopMotor();
  }
}
