// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.math.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.math.util.Units;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.RobotBase;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.motorcontrol.MotorControllerGroup;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.components.DriveGearBox;
import frc.robot.components.Gyroscope;
import frc.robot.networktables.DriveTrainTable;
import frc.robot.simulation.simsystems.DriveTrainSim;
import frc.robot.Constants.CAN;
import frc.robot.Constants.SolenoidChannel;

import static frc.robot.Constants.DriveConstants.*;

import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

public class DriveTrainSubsystem extends SubsystemBase {

  // Motors
  private CANSparkMax leftMotor;
  private CANSparkMax leftMotorFollower;
  private CANSparkMax rightMotor;
  private CANSparkMax rightMotorFollower;

  private RelativeEncoder leftEncoder;
  private RelativeEncoder rightEncoder;

  private MotorControllerGroup leftMotorGroup;
  private MotorControllerGroup rightMotorGroup;

  // Solenoid
  private DoubleSolenoid shifterSolenoid;

  // Gyroscope
  private Gyroscope gyroscope;
  
  // Control
  private SimpleMotorFeedforward highGearFeedforward;
  private SimpleMotorFeedforward lowGearFeedforward;

  private DifferentialDriveOdometry odometry;

  private DriveGearBox gearBox;

  // Network Tables
  private DriveTrainTable table;
  private Field2d field;

  // Simulation
  private DriveTrainSim sim;

  /** Creates a new DriveTrainSubsystem. */
  public DriveTrainSubsystem(Gyroscope robotGyroscope) {
    
    leftMotor = new CANSparkMax(CAN.kDriveLeftMotor, MotorType.kBrushless);
    leftMotorFollower = new CANSparkMax(CAN.kDriveLeftSecondaryMotor, MotorType.kBrushless);
    rightMotor = new CANSparkMax(CAN.kDriveRightMotor, MotorType.kBrushless);
    rightMotorFollower = new CANSparkMax(CAN.kDriveRightSecondaryMotor, MotorType.kBrushless);

    // setup encoders
    leftEncoder = leftMotor.getEncoder();
    rightEncoder = rightMotor.getEncoder();

    // Because to make both sides go the same direction the right side must be inverted
    leftMotor.setInverted(true);
    leftMotorFollower.setInverted(true);
    rightMotor.setInverted(false);
    rightMotorFollower.setInverted(false);
    setCoastMode();

    leftMotorGroup = new MotorControllerGroup(leftMotor, leftMotorFollower);
    rightMotorGroup = new MotorControllerGroup(rightMotor, rightMotorFollower);

    shifterSolenoid = new DoubleSolenoid(CAN.kPneumaticHub, PneumaticsModuleType.REVPH, SolenoidChannel.kDriveShifterHighGear, SolenoidChannel.kDriveShifterLowGear);
    // initial position is low gear
    shifterSolenoid.set(Value.kReverse);

    gearBox = new DriveGearBox(kHighGearRatio, kLowGearRatio, kWheelDiameter/2, leftEncoder.getPosition(), rightEncoder.getPosition());

    // used for calculating theoretical voltages to drive the motors
    highGearFeedforward = new SimpleMotorFeedforward(kDriveBaseVolts, kDriveVoltsSecondsPerMeterHighGear, kDriveVoltsSecondsSquaredPerMeterHighGear);
    lowGearFeedforward = new SimpleMotorFeedforward(kDriveBaseVolts, kDriveVoltsSecondsPerMeterLowGear, kDriveVoltsSecondsSquaredPerMeterLowGear);

    gyroscope = robotGyroscope;

    // Used for estimating Robot Position 
    odometry = new DifferentialDriveOdometry(
      new Rotation2d(getAngle()), 
      getLeftDistance(), 
      getRightDistance(),
      new Pose2d()
    );

    // data
    table = new DriveTrainTable();
    field = new Field2d();
    SmartDashboard.putData(field);

    // simulation
    if (RobotBase.isSimulation()) {
      sim = new DriveTrainSim(leftMotor, leftMotorFollower, rightMotor, rightMotorFollower, gyroscope.getSim());
    }
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    gearBox.update(leftEncoder.getPosition(), rightEncoder.getPosition());

    // estimate robot position
    odometry.update(
      new Rotation2d(getAngle()),
      getLeftDistance(), 
      getRightDistance()
    );

    // display estimated position
    field.setRobotPose(getPose());

    // Updates the gyro for it
    gyroscope.updateGyroTable();

    // Update Network Tables
    table.updateMotorTable(0, leftMotor);
    table.updateMotorTable(1, leftMotorFollower);
    table.updateMotorTable(2, rightMotor);
    table.updateMotorTable(3, rightMotorFollower);

    table.setShifterPosition(inHighGear());
  }

  @Override
  public void simulationPeriodic() {
    sim.update();
  }

  public void setHighGear() {
    // simulation
    if (RobotBase.isSimulation()) {
      sim.setShifterForward();
    }

    shifterSolenoid.set(Value.kForward);
    gearBox.setHighGear();
  }

  public void setLowGear() {
    // simulation
    if (RobotBase.isSimulation()) {
      sim.setShifterReverse();
    }

    shifterSolenoid.set(Value.kReverse);
    gearBox.setLowGear();
  }

  public boolean inHighGear() {
    // simulation
    if (RobotBase.isSimulation()) {
      return (sim.getShifter() == Value.kForward);
    }

    return (shifterSolenoid.get() == Value.kForward);
  }

  public boolean inLowGear() {
    // simulation
    if (RobotBase.isSimulation()) {
      return (sim.getShifter() == Value.kReverse);
    }

    return (shifterSolenoid.get() == Value.kReverse);
  }

  /**
   * Used for controlling the drive with raw voltages. To set the speed of the
   * left and right sides of the drive use setWheelSpeeds instead
   * 
   * @param leftVolts Voltage to drive the left motor at. Must be less than 12V
   * @param rightVolts Voltage to drive the right motor at. Must be less than 12V
   */
  public void tankDriveVolts(double leftVolts, double rightVolts) {
    if (leftVolts > 12.0){ 
      leftVolts = 12.0;
    }
    if (rightVolts > 12.0){ 
      rightVolts = 12.0;
    }
    
    leftMotorGroup.setVoltage(leftVolts);
    rightMotorGroup.setVoltage(rightVolts);
  }
  
  /**
   * Primary method for controlling the drive with wheel velocities. Wheel speeds are
   * used to calculate theoretical voltages to drive the robot chassis by Feedforward
   * controllers. No closed loop control is performed
   * 
   * @param wheelSpeeds The left and right wheel speeds of the robot in m/s
   */
  public void setWheelSpeeds(DifferentialDriveWheelSpeeds wheelSpeeds) {

    if (inLowGear()) {
      tankDriveVolts(
        lowGearFeedforward.calculate(wheelSpeeds.leftMetersPerSecond), 
        lowGearFeedforward.calculate(wheelSpeeds.rightMetersPerSecond)
      );
    }
    else {
      tankDriveVolts(
        highGearFeedforward.calculate(wheelSpeeds.leftMetersPerSecond), 
        highGearFeedforward.calculate(wheelSpeeds.rightMetersPerSecond)
      );
    }

    // Update Network Tables
    table.setVelocitySetpoint(0, wheelSpeeds.leftMetersPerSecond);
    table.setVelocitySetpoint(1, wheelSpeeds.leftMetersPerSecond);
    table.setVelocitySetpoint(2, wheelSpeeds.rightMetersPerSecond);
    table.setVelocitySetpoint(3, wheelSpeeds.rightMetersPerSecond);
  }

  public SimpleMotorFeedforward getLowGearFeedforward() {
    return lowGearFeedforward;
  }

  public SimpleMotorFeedforward getHightGearFeedforward() {
    return highGearFeedforward;
  }

  public void setBreakMode() {
    leftMotor.setIdleMode(IdleMode.kBrake);
    leftMotorFollower.setIdleMode(IdleMode.kBrake);
    rightMotor.setIdleMode(IdleMode.kBrake);
    rightMotorFollower.setIdleMode(IdleMode.kBrake);
  }

  public void setCoastMode() {
    leftMotor.setIdleMode(IdleMode.kCoast);
    leftMotorFollower.setIdleMode(IdleMode.kCoast);
    rightMotor.setIdleMode(IdleMode.kCoast);
    rightMotorFollower.setIdleMode(IdleMode.kCoast);
  }

  /**
   * Resets the Odometry to a given pose on the field
   * 
   * @param pose the pose to reset the odometry to
   */
  public void resetOdometry(Pose2d pose) {
    odometry.resetPosition(
      new Rotation2d(getAngle()),
      getLeftDistance(),
      getRightDistance(),
      pose);
  }

  /**
   * 
   * @return the estimated position of the robot in meters and radians
   */
  public Pose2d getPose() {
    return odometry.getPoseMeters();
  }

  /**
   * 
   * @return angle of the drive in radians
   */
  public double getAngle() {
    return Units.degreesToRadians(gyroscope.getAngle());
  }

  /**
   * 
   * @return the velocities in m/s of the left and right sides of the robot
   */
  public DifferentialDriveWheelSpeeds getWheelSpeeds() {
    return new DifferentialDriveWheelSpeeds(getLeftVelocity(), getRightVelocity());
  }

  /**
   * 
   * @return the velocity of the left side of the robot in m/s
   */
  public double getLeftVelocity() {
    if (inLowGear()) {
      return Units.rotationsPerMinuteToRadiansPerSecond(leftEncoder.getVelocity() / kLowGearRatio) * kWheelDiameter/2;
    } else {
      return Units.rotationsPerMinuteToRadiansPerSecond(leftEncoder.getVelocity() / kHighGearRatio) * kWheelDiameter/2;
    }
  }

  /**
   * 
   * @return the velocity of the right side of the robot in m/s
   */
  public double getRightVelocity() {
    if (inLowGear()) {
      return Units.rotationsPerMinuteToRadiansPerSecond(rightEncoder.getVelocity() / kLowGearRatio) * kWheelDiameter/2;
    } else {
      return Units.rotationsPerMinuteToRadiansPerSecond(rightEncoder.getVelocity() / kHighGearRatio) * kWheelDiameter/2;
    }
  }

  /**
   * 
   * @return the distance the left side of the drive has traveled in meters
   */
  public double getLeftDistance() {
    return gearBox.getLeftMeters();
  }

  /**
   * 
   * @return the distance the right side of the drive has traveled in meters
   */
  public double getRightDistance() {
    return gearBox.getRightMeters();
  }

  public Field2d getField() {
    return field;
  }
}