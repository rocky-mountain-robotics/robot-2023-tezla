// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.math.controller.ArmFeedforward;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.RobotBase;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;


import frc.robot.Constants.CAN;
import frc.robot.Constants.Control;
import frc.robot.networktables.ArmTable;
import frc.robot.simulation.simsystems.ArmSim;

import static frc.robot.Constants.DIO.*;


import static frc.robot.Constants.ArmConstants.*;

import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import edu.wpi.first.wpilibj.DigitalInput;

public class ArmSubsystem extends SubsystemBase {

  private ShoulderSubsystem shoulderSubsystem;

  private CANSparkMax jointMotor;
  private RelativeEncoder encoder;

  private ArmFeedforward feedforward;

  private DigitalInput limitSwitch;

  private ArmTable table;

  private ArmSim sim;

  /** Creates a new ArmSubsystem. */
  public ArmSubsystem(ShoulderSubsystem robotShoulderSubsystem) {

    shoulderSubsystem = robotShoulderSubsystem;

    jointMotor = new CANSparkMax(CAN.kArmJointMotor, MotorType.kBrushless);
    encoder = jointMotor.getEncoder();
    encoder.setPosition(-kGearRatio/2);

    feedforward = new ArmFeedforward(kStatic, kGravity, kVelocity);

    table = new ArmTable();

    limitSwitch = new DigitalInput(kArmLimitSwitch);

    if (RobotBase.isSimulation()) {
      sim = new ArmSim(jointMotor);
    }
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    table.setArmAngle(getLowerArmPosition());
    table.setArmAngularVelocity(getLowerArmVelocity());

    table.setArmVoltage(jointMotor.getAppliedOutput());
    table.setArmVoltage(jointMotor.getAppliedOutput() * 12.0);

    table.setShoulderExtended(shoulderSubsystem.isShoulderOut());

    SmartDashboard.putBoolean("Switch Pressed", limitSignalReceived());

    //If limit switch on arm is pressed, set arm position to -90 through encoder position -90 arm position = -20 encoder position
    if (limitSignalReceived()) {
      encoder.setPosition(-kGearRatio/2);
    }
  }

  @Override
  public void simulationPeriodic() {
    sim.update();
  }

  public boolean limitSignalReceived() {
    return !limitSwitch.get();
  }

  /**
   * Sets the voltage on the arm motor within safe parameters
   * @param volts must be less than or equal to 1.32 (volts)
   */
  public void setVoltage(double volts) {
    // at height limit
    if(getLowerArmPosition() > Control.kMaxArmAngle) {
      if (volts > feedforward.calculate(Units.degreesToRadians(getLowerArmPosition()), 0)) {
        volts = feedforward.calculate(Units.degreesToRadians(getLowerArmPosition()), 0);
      }
    }

    // Max voltage is 3 Volts
    if(Math.abs(volts) > 3.0) {
      volts = Math.copySign(3.0, volts);
    }
    
    jointMotor.setVoltage(volts);
  }

  /**
   * sets the speed of the arm with feedforward
   * 
   * @param  / kGearRatio the number of degrees per second
   */
  public void setSpeed(double velocity) {
    setVoltage(feedforward.calculate(Units.degreesToRadians(getLowerArmPosition()), velocity));
  }

  /**
   * 
   * @return standard feedforward for controlling the arm
   */
  public ArmFeedforward getFeedforward() {
    return feedforward;
  }

  /**
   * Sets the position of the Arm based on position measured by limit switches or absolute encoders.
   * The position is in reference to the horizontal, so 0 is parallel to the ground
   * 
   * @param positionRadians the position of the arm in degrees.
   */
  public void setLowerArmPosition(double positionDegrees) {
    encoder.setPosition(Units.degreesToRotations((positionDegrees - shoulderSubsystem.getUpperArmPosition())) * kGearRatio);
  }

  /**
   * Arm position is measured in reference to the arm parallel to the ground
   * 
   * @return the position of the arm in degrees (deg)
   */
  public double getLowerArmPosition() {
    return shoulderSubsystem.getUpperArmPosition() + Units.rotationsToDegrees(encoder.getPosition() / kGearRatio);
  }

  /**
   * Max velocity should be 30 degrees per second
   * 
   * @return the velocity of the arm in degrees per second (deg/s)
   */
  public double getLowerArmVelocity() {
    return Units.radiansToDegrees(
      Units.rotationsPerMinuteToRadiansPerSecond(
        encoder.getVelocity()) / kGearRatio);
  }
}

// PAT: glpat-zy5X8zCFDnuiyP5r-hJy
