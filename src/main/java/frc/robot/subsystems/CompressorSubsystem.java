// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import static frc.robot.Constants.CAN;

public class CompressorSubsystem extends SubsystemBase {

  Compressor compressor; 

  /** Creates a new CompressorSubsystem. */
  public CompressorSubsystem() {

    compressor = new Compressor(CAN.kPneumaticHub, PneumaticsModuleType.REVPH);
    disable();
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }

  public void enable() {
    compressor.enableDigital();
  }

  public void disable() {
    compressor.disable();
  }

  public CommandBase getEnableCommand() {
    return this.runOnce(() -> {
      enable();
    });
  }

  public CommandBase getDisableCommand() {
    return this.runOnce(() -> {
      disable();
    });
  }

}
