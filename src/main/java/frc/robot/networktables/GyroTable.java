// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.networktables;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;

/** Add your docs here. */
public class GyroTable {

  private NetworkTable gyroTable;

  private NetworkTableEntry primaryGyroAngle;
  private NetworkTableEntry primaryGyroRate;

  private NetworkTableEntry accelX;
  private NetworkTableEntry accelY;
  private NetworkTableEntry accelZ;

  
  private NetworkTableEntry gyroAngleX;
  private NetworkTableEntry gyroAngleY;

  public GyroTable() {
    gyroTable = NetworkTableInstance.getDefault().getTable("Gyro");

    primaryGyroAngle = gyroTable.getEntry("Angle");
    primaryGyroRate = gyroTable.getEntry("Angle Rate");

    accelX = gyroTable.getEntry("Accel X");
    accelY = gyroTable.getEntry("Accel Y");
    accelZ = gyroTable.getEntry("Accel Z");

    gyroAngleX = gyroTable.getEntry("Roll Angle X");
    gyroAngleY = gyroTable.getEntry("Pitch Angle Y"); // balance angle
  }

  public void setPrimaryGyroAngle(double value) {
    primaryGyroAngle.setDouble(value);
  }

  public void setPrimaryGyroAngleRate(double value) {
    primaryGyroRate.setDouble(value);
  }

  public void setAccelX(double value) {
    accelX.setDouble(value);
  }

  public void setAccelY(double value) {
    accelY.setDouble(value);
  }

  public void setAccelZ(double value) {
    accelZ.setDouble(value);
  }

  public void setGyroAngleX(double value) {
    gyroAngleX.setDouble(value);
  }

  public void setGyroAngleY(double value) {
    gyroAngleY.setDouble(value);
  }
}
