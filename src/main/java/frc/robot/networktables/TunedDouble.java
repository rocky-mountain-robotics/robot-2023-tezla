// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.networktables;

import edu.wpi.first.util.sendable.Sendable;
import edu.wpi.first.util.sendable.SendableBuilder;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/** Add your docs here. */
public class TunedDouble implements Sendable {

  private double value;

  public TunedDouble(String key, double value) {

    this.value = value;
    SmartDashboard.putData(key, this);;
  }

  @Override
  public void initSendable(SendableBuilder builder) {
    builder.setSmartDashboardType("TunedDouble");
    builder.addDoubleProperty("value", this::getValue, this::setValue);
  }

  public double getValue(){
    return value;
  }

  public void setValue(double value){
    this.value = value;
  }

  



}
