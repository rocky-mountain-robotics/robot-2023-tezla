// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.networktables;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BooleanSupplier;
import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/** Add your docs here. */
public class ShuffleboardTool {
  public static final ShuffleboardTool instance = new ShuffleboardTool();

  public static ShuffleboardTool getInstance() {
    return instance;
  }

  private HashMap<String, DoubleSupplier> doubleWatchers = new HashMap<String, DoubleSupplier>();
  private HashMap<String, BooleanSupplier> booleanWatchers = new HashMap<String, BooleanSupplier>();

  public void run() {
    for (Map.Entry<String, DoubleSupplier> set : doubleWatchers.entrySet()) {
      SmartDashboard.putNumber(set.getKey(), set.getValue().getAsDouble());

    }

    for (Map.Entry<String, BooleanSupplier> set : booleanWatchers.entrySet()) {
      SmartDashboard.putBoolean(set.getKey(), set.getValue().getAsBoolean());
    }
  }

  public void addDebug(String name, DoubleSupplier supplier) {
    doubleWatchers.put(name, supplier);
  }

  public void addDebug(String name, BooleanSupplier supplier) {
    booleanWatchers.put(name, supplier);
  }
}
