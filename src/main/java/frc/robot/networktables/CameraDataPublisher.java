// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.networktables;

import edu.wpi.first.math.geometry.Transform3d;
import edu.wpi.first.networktables.DoublePublisher;
import edu.wpi.first.networktables.IntegerPublisher;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.networktables.PubSubOption;

/** Add your docs here. */
public class CameraDataPublisher {

  private final NetworkTable cameraTable;

  private final IntegerPublisher cameraFrameTime;

  private final DoublePublisher cameraPosX;
  private final DoublePublisher cameraPosY;
  private final DoublePublisher cameraPosZ;
  
  private final DoublePublisher cameraRotation;

  private final DoublePublisher cameraRotationX;
  private final DoublePublisher cameraRotationY;
  private final DoublePublisher cameraRotationZ;



  public CameraDataPublisher() {
    cameraTable = NetworkTableInstance.getDefault().getTable("Camera");

    cameraFrameTime = cameraTable.getIntegerTopic("frame time").publish(PubSubOption.keepDuplicates(true));

    cameraPosX = cameraTable.getDoubleTopic("X").publish(PubSubOption.keepDuplicates(true));
    cameraPosY = cameraTable.getDoubleTopic("Y").publish(PubSubOption.keepDuplicates(true));
    cameraPosZ = cameraTable.getDoubleTopic("Z").publish(PubSubOption.keepDuplicates(true));

    cameraRotation = cameraTable.getDoubleTopic("Rotation").publish(PubSubOption.keepDuplicates(true));
    cameraRotationX = cameraTable.getDoubleTopic("Rot X").publish(PubSubOption.keepDuplicates(true));
    cameraRotationY = cameraTable.getDoubleTopic("Rot Y").publish(PubSubOption.keepDuplicates(true));
    cameraRotationZ = cameraTable.getDoubleTopic("Rot Z").publish(PubSubOption.keepDuplicates(true));
  }

  public void publish(Transform3d transform3d, long frameTime) {
    cameraFrameTime.set(frameTime);

    cameraPosX.set(transform3d.getX());
    cameraPosY.set(transform3d.getY());
    cameraPosZ.set(transform3d.getZ());

    cameraRotation.set(transform3d.getRotation().getAngle());

    cameraRotationX.set(transform3d.getRotation().getX());
    cameraRotationY.set(transform3d.getRotation().getY());
    cameraRotationZ.set(transform3d.getRotation().getZ());
  }
}
