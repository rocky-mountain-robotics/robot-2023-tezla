// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.networktables;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;

/** Add your docs here. */
public class IntakeTable {
	private NetworkTable intakeTable;

	private NetworkTableEntry intakeStatus;
	private NetworkTableEntry pressureAmt;

	public IntakeTable() {
		intakeTable = NetworkTableInstance.getDefault().getTable("Intake");

		intakeStatus = intakeTable.getEntry("Intake Status");
    	pressureAmt = intakeTable.getEntry("Pressure");
	}

	public void setIntakePosition(boolean value) {
		intakeStatus.setBoolean(value);
	}
	
	public void setPressure(int value) {
		pressureAmt.setInteger(value);
	}
}
