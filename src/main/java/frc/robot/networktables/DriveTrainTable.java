// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.networktables;

import java.util.Map;

import com.revrobotics.CANSparkMax;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInWidgets;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;

/**An object that holds the Network Table values for an individual motor.
 * @param parentTable The Network Table that the motor subtable lives in.
 * @param name The name for the motor, used in the Network Table subtable name.
*/
public class DriveTrainTable {

  private class MotorTable {
    public NetworkTable motorTable;

    public NetworkTableEntry voltage;
    public NetworkTableEntry encoderPosition;
    public NetworkTableEntry encoderVelocity;
    public NetworkTableEntry velocitySetpoint;

    public MotorTable(NetworkTable parentTable, String name) {
      motorTable = parentTable.getSubTable(name);

      voltage = motorTable.getEntry("Voltage");
      encoderPosition = motorTable.getEntry("Encoder Position");
      encoderVelocity = motorTable.getEntry("Encoder Velocity");
      velocitySetpoint = motorTable.getEntry("Velocity Setpoint");
    }
  }

  private MotorTable[] motorTables;


  private NetworkTable driveTable;

  // private NetworkTableEntry motorTables[0].voltage;
  // private NetworkTableEntry motorTables[2].voltage;

  // private NetworkTableEntry leftEncoderPosition;
  // private NetworkTableEntry rightEncoderPosition;
  // private NetworkTableEntry motorTables[0].encoderVelocity;
  // private NetworkTableEntry rightEncoderVelocity;

  private NetworkTableEntry shifterPosition;

  // private NetworkTableEntry leftVelocitySetpoint;
  // private NetworkTableEntry rightVelocitySetpoint;

  public DriveTrainTable() {
    driveTable = NetworkTableInstance.getDefault().getTable("DriveTrain");

    motorTables = new MotorTable[4];

    motorTables[0] = new MotorTable(driveTable, "Left Back Motor");
    motorTables[1] = new MotorTable(driveTable, "Left Front Motor");
    motorTables[2] = new MotorTable(driveTable, "Right Back Motor");
    motorTables[3] = new MotorTable(driveTable, "Right Front Motor");

    // motorTables[0].voltage = driveTable.getEntry("Left Voltage");
    // motorTables[2].voltage = driveTable.getEntry("Right Voltage");
    // leftEncoderPosition = driveTable.getEntry("Left Encoder Position");
    // rightEncoderPosition = driveTable.getEntry("Right Encoder Position");
    // motorTables[0].encoderVelocity = driveTable.getEntry("Left Encoder Velocity");
    // rightEncoderVelocity = driveTable.getEntry("Right Encoder Velocity");
    // leftVelocitySetpoint = driveTable.getEntry("Left Velocity Setpoint");
    // rightVelocitySetpoint = driveTable.getEntry("Right Velocity Setpoint");

    shifterPosition = driveTable.getEntry("In High Gear");

    // setupShuffleBoard();
  }

/**
 * 
 * @param entry the table entry to edit. Can be voltage, 
 * @param motor a MotorTable to edit
 * @param value the value of the updated table entry
 */
  public void setTable(String entry, MotorTable motor, double value) {
    if(entry == "voltage") {
      motor.voltage.setDouble(value);
    }
  }

  public void updateMotorTable(int index, CANSparkMax motor) {
    MotorTable motorTable = motorTables[index];
    
    motorTable.voltage.setDouble(motor.getAppliedOutput());
    motorTable.encoderPosition.setDouble(motor.getEncoder().getPosition());
    motorTable.encoderVelocity.setDouble(motor.getEncoder().getVelocity());
  }

  /**
   * 
   * @param value true if in high gear
   */
  public void setShifterPosition(boolean value) {
    shifterPosition.setBoolean(value);
  }

  public void setVelocitySetpoint(int index, double value) {
    motorTables[index].velocitySetpoint.setDouble(value);
  }


  public void setupShuffleBoard() {
    ShuffleboardTab driveTab = Shuffleboard.getTab("DriveTrain");


    driveTab.addDouble("Left", () -> {return Math.abs(motorTables[0].voltage.getDouble(0));})
      .withWidget(BuiltInWidgets.kVoltageView)
      .withProperties(Map.of("Max", 12, "Orientation", "VERTICAL"))
      .withPosition(0, 0)
      .withSize(1, 5);
    driveTab.addDouble("Left Graph", () -> {return motorTables[0].voltage.getDouble(0);})
      .withWidget(BuiltInWidgets.kGraph)
      .withPosition(1, 0)
      .withSize(5, 5);

    driveTab.addDouble("Left Encoder Velocity", () -> {return motorTables[0].encoderVelocity.getDouble(0);})
      .withWidget(BuiltInWidgets.kDial)
      .withProperties(Map.of("Min", -3, "Max", 3))
      .withPosition(0, 5)
      .withSize(3, 3);
    driveTab.addDouble("Left Velocity Setpoint", () -> {return motorTables[0].velocitySetpoint.getDouble(0);})
      .withWidget(BuiltInWidgets.kDial)
      .withProperties(Map.of("Min", -3, "Max", 3))
      .withPosition(3, 5)
      .withSize(3, 3);
    
    driveTab.addDouble("Right", () -> {return Math.abs(motorTables[2].voltage.getDouble(0));})
      .withWidget(BuiltInWidgets.kVoltageView)
      .withProperties(Map.of("Max", 12, "Orientation", "VERTICAL"))
      .withPosition((6 + 6) + 0, 0)
      .withSize(1, 5);
    driveTab.addDouble("Right Graph", () -> {return motorTables[2].voltage.getDouble(0);})
      .withWidget(BuiltInWidgets.kGraph)
      .withPosition((6 + 6) + 1, 0)
      .withSize(5, 5);
    
    driveTab.addDouble("Right Encoder Velocity", () -> {return motorTables[2].encoderVelocity.getDouble(0);})
      .withWidget(BuiltInWidgets.kDial)
      .withProperties(Map.of("Min", -3, "Max", 3))
      .withPosition((6 + 6) + 0, 5)
      .withSize(3, 3);
    driveTab.addDouble("Right Velocity Setpoint", () -> {return motorTables[2].velocitySetpoint.getDouble(0);})
      .withWidget(BuiltInWidgets.kDial)
      .withProperties(Map.of("Min", -3, "Max", 3))
      .withPosition((6 + 6) + 3, 5)
      .withSize(3, 3);
  }
}
