// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.networktables;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;

/** Add your docs here. */
public class ArmTable {

  private NetworkTable armTable;

  private NetworkTableEntry armVolts;

  private NetworkTableEntry armAngle;
  private NetworkTableEntry armAngularVelocity;

  private NetworkTableEntry shoulderExtended;

  public ArmTable() {
    armTable = NetworkTableInstance.getDefault().getTable("Arm");

    armVolts = armTable.getEntry("Volts");

    armAngle = armTable.getEntry("Angle");
    armAngularVelocity = armTable.getEntry("AngularVelocity");

    shoulderExtended = armTable.getEntry("Piston Extended");
  }

  public void setArmVoltage(double value) {
    armVolts.setDouble(value);
  }

  public void setArmAngle(double value) {
    armAngle.setDouble(value);
  }

  public void setArmAngularVelocity(double value) {
    armAngularVelocity.setDouble(value);
  }

  public void setShoulderExtended(boolean value) {
    shoulderExtended.setBoolean(value);
  }
}
