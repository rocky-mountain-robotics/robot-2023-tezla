// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.components;

import edu.wpi.first.math.util.Units;

/** Class for accumulating distance of a shifting gearbox */
public class DriveGearBox {

  private final double m_highGearRatio;
  private final double m_lowGearRatio;
  private final double m_wheelRadiusMeters;

  private double m_gearRatio;

  private double leftPositionMeters;
  private double rightPositionMeters;

  private double lastLeftEncoderRotations;
  private double lastRightEncoderRotations;

  public DriveGearBox(final double highGearRatio, final double lowGearRatio, final double wheelRadiusMeters,
      double leftEncoderRotations, double rightEncoderRotations) {
    
    m_highGearRatio = highGearRatio;
    m_lowGearRatio = lowGearRatio;
    m_wheelRadiusMeters = wheelRadiusMeters;

    m_gearRatio = lowGearRatio;

    leftPositionMeters = 0;
    rightPositionMeters = 0;

    lastLeftEncoderRotations = leftEncoderRotations;
    lastRightEncoderRotations = rightEncoderRotations;
  }

  /**
   * updates the left and right distances
   * 
   * @param leftEncoderRotations the number of rotations of the left encoder
   * @param rightEncoderRotations the number of rotations of the right encoder
   */
  public void update(double leftEncoderRotations, double rightEncoderRotations) {
    double leftEncoderDelta = leftEncoderRotations - lastLeftEncoderRotations;
    double rightEncoderDelta = rightEncoderRotations - lastRightEncoderRotations;

    lastLeftEncoderRotations = leftEncoderRotations;
    lastRightEncoderRotations = rightEncoderRotations;

    leftPositionMeters += Units.rotationsToRadians(leftEncoderDelta / m_gearRatio) * m_wheelRadiusMeters;
    rightPositionMeters += Units.rotationsToRadians(rightEncoderDelta / m_gearRatio) * m_wheelRadiusMeters;
  }

  public double getRightMeters() {
    return rightPositionMeters;
  }

  public double getLeftMeters() {
    return leftPositionMeters;
  }

  /** Update the gearbox to high gear */
  public void setHighGear() {
    m_gearRatio = m_highGearRatio;
  }

  /** Update the gearbox to low gear */
  public void setLowGear() {
    m_gearRatio = m_lowGearRatio;
  }
}
