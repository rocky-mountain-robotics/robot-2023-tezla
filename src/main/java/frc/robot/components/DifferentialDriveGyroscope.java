// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.components;

import edu.wpi.first.math.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.math.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.Timer;

/** 
 * Calculates the angle of the robot based on the wheel velocities.
 * update() must be called in periodic for gyro to remain accurate
 */
public class DifferentialDriveGyroscope {

  private DifferentialDriveKinematics driveKinematics;

  private double lastUpdateTime;

  private double angle;

  public DifferentialDriveGyroscope(double trackWidth) {
    driveKinematics = new DifferentialDriveKinematics(trackWidth);

    lastUpdateTime = Timer.getFPGATimestamp();

    angle = 0.0;
  }

  /**
   * 
   * @return the angle of the  gyroscope in radians
   */
  public double getAngle() {
    return angle;
  }

  /**
   * 
   * @param angle angle of the gyro in radians
   */
  public void resetGyro(double angle) {
    this.angle = angle;
  }

  /**
   * 
   * @param angle angle of the gyro in degrees
   */
  public void resetGyroDegrees(double angle) {
    this.angle = Units.degreesToRadians(angle);
  }

  /**
   * must be run as frequently as possible for the gyro to be accurate
   * 
   * @param leftVelocity velocity of the left side of the drive in meters per second
   * @param rightVelocity velocity of the right side of the drive in meters per second
   */
  public void update(double leftVelocity, double rightVelocity) {
    double deltaTime = Timer.getFPGATimestamp() - lastUpdateTime;
    lastUpdateTime = Timer.getFPGATimestamp();

    angle += driveKinematics.toChassisSpeeds(
      new DifferentialDriveWheelSpeeds(leftVelocity, rightVelocity)
      ).omegaRadiansPerSecond * deltaTime;
  }
}
