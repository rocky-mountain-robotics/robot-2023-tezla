// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.components;

import edu.wpi.first.wpilibj.DigitalInput;

import frc.robot.Constants.DIO;

/** Class representing the switch which controls the compressor */
public class CompressorSwitch {

  DigitalInput compressorSwitch;

  public CompressorSwitch(){
    compressorSwitch = new DigitalInput(DIO.kCompressorSwitch);
  }

  public boolean isOn(){
    return !compressorSwitch.get();
  }
}


