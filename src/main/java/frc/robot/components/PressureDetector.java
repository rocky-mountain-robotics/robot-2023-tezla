// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.components;

import edu.wpi.first.wpilibj.AnalogInput;
import frc.robot.Constants.AnalogIO;

/** Add your docs here. */
public class PressureDetector {
  AnalogInput input;

  public PressureDetector() {
    input = new AnalogInput(AnalogIO.kPressureSensor);
  }

  /**
   * 
   * @return the pressure stored in PSI
   */
  public double getPressurePSI() {
    return (getSensorValue() - 400) * (2100 - 400) / (110.0);
  }

  /**
   * 400 is 0 psi
   * 870 is 30 psi
   * 2100 is 110 psi
   * 
   * @return the sensors raw reading
   */
  public int getSensorValue() {
    return input.getValue();
	}

  public boolean atPressure() {
    return input.getValue() >= 870;
  }
}
