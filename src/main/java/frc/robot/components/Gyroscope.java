// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.components;

import edu.wpi.first.wpilibj.SPI.Port;
import frc.robot.networktables.GyroTable;
import frc.robot.wpilibj.ADIS16470_IMU;
import frc.robot.wpilibj.ADIS16470_IMUSim;
import frc.robot.wpilibj.ADIS16470_IMU.CalibrationTime;
import frc.robot.wpilibj.ADIS16470_IMU.IMUAxis;

/** An interface to the gyroscope on the robot */
public class Gyroscope {

  // Create the Gyro class to reference
  private ADIS16470_IMU gyro;

  private GyroTable table = new GyroTable();

  // Constructor
  public Gyroscope(){
    gyro = new ADIS16470_IMU(IMUAxis.kZ, Port.kOnboardCS0, CalibrationTime._4s);
    table = new GyroTable();
  }

  /**
   * 
   * @return Gyro axis angle in degrees
   */
  public double getAngle() {
    return gyro.getAngle();
  }

  /**
   * Reference angle of 0 degrees is balanced.
   * 
   * @return the balance angle in degrees (between -180 and 180)
   */
  public double getBalanceAngle() {
    // angle will be between 0 and 360
    double angle = gyro.getYComplementaryAngle();

    if (angle > 180) {
      return angle - 360;
    }
    else {
      return angle;
    }
  }

  /**
   * 
   * @return the rate of change of the balance angle in degrees per second
   */
  public double getBalanceRate() {
    return gyro.getRateX();
  }

  public void updateGyroTable() {
    table.setPrimaryGyroAngle(gyro.getAngle());
    table.setPrimaryGyroAngleRate(gyro.getRate());

    table.setAccelX(gyro.getAccelX());
    table.setAccelY(gyro.getAccelY());
    table.setAccelZ(gyro.getAccelZ());

    table.setGyroAngleX(gyro.getXComplementaryAngle());
    table.setGyroAngleY(gyro.getYComplementaryAngle());
  }

  public ADIS16470_IMUSim getSim() {
    return new ADIS16470_IMUSim(gyro);
  }
}
