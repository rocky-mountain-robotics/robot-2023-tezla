// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

/*
 * Editors:
 * Nathan George
 * Caleb Borlin
 * Andrew George
 */

package frc.robot;

import edu.wpi.first.math.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.math.util.Units;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {

  /**
   * Constants for the Joysticks used to control the robot
   */
  public static final class StickOfJoy {
    // button constants
    public static final int kTriggerButton = 1;
    public static final int kThumbButton = 2;
    public static final int kTopButton3 = 3;
    public static final int kTopButton4 = 4;
    public static final int kTopButton5 = 5;
    public static final int kTopButton6 = 6;
    public static final int kBaseButton7 = 7;
    public static final int kBaseButton8 = 8;
    public static final int kBaseButton9 = 9;
    public static final int kBaseButton10 = 10;
    public static final int kBaseButton11 = 11;
    public static final int kBaseButton12 = 12;
  }

  public static class OperatorConstants {
    public static final int kDriverControllerPort = 0;
    public static final int kManipulatorControllerPort = 1;
  }

  /**
   * CAN IDs for all devices go here
   */
  public static class CAN {

    // RoboRIO
    public static final int kRoboRio = 0;

    // Power Distribution Hub
    public static final int kPowerDistributionHub = 1;

    // Pneumatic Hub
    public static final int kPneumaticHub = 2;

    //Drive Train
    /** CAN ID for the Spark Max controlling the main left motor of the drive train */
    public static final int kDriveLeftMotor = 6;
    /** CAN ID for the Spark Max controlling the secondary left motor of the drive train */
    public static final int kDriveLeftSecondaryMotor = 7;

    /** CAN ID for the Spark Max controlling the main right motor of the drive train */
    public static final int kDriveRightMotor = 9;
    /** CAN ID for the Spark Max controlling the secondary right motor of the drive train */
    public static final int kDriveRightSecondaryMotor = 8;

    // Arm

    /** CAN ID for the Spark Max controlling the joint motor in the arm */
    public static final int kArmJointMotor = 11;

    // Intake

    /** CAN ID for the Spark Max controlling the intake motor */
    public static final int kIntakeMotorUpper = 12;
    public static final int kIntakeMotorLower = 13;
  }

  /**
   * All Solenoid Channel IDs go here
   */
  public static final class SolenoidChannel {
    
    // Drive Train
    public static final int kDriveShifterHighGear = 15;
    public static final int kDriveShifterLowGear = 14;

    // Arm
    public static final int kArmPivotForward = 11;
    public static final int kArmPivotReverse = 12;

    // Gripper
    public static final int kGripperClose = 7;
    public static final int kGripperOpen = 8;

    // Intake
    public static final int kIntakeForward = 10;
    public static final int kIntakeReverse = 9;
  }

  /**
   * All DIgital Input/Output constants go here
   */
  public static final class DIO {

    /** Digital input of compressor switch */
    public static final int kCompressorSwitch = 0;

    // TODO: figure out channel
    public static final int kArmLimitSwitch = 1;
  }

  public static final class AnalogIO {
    public static final int kPressureSensor = 0;
  }

  public static final class AutoConstants {
    // Reasonable baseline values for a RAMSETE follower in units of meters and seconds
    public static final double kRamseteB = 0.1;
    public static final double kRamseteZeta = 0.7;

    // pid for following a trajectory
    //public static final double kDriveKp = 6.2162;
    public static final double kDriveKp = 0;

    public static final double kMaxSpeedMetersPerSecond = 0.6;
    public static final double kMaxAccelerationMetersPerSecondSquared = 0.3;
  }

  public static final class Control {
    /** Max speed of the robot in meters / second */
    public static final double kMaxSpeedLowGear = 1.0;
    public static final double kMaxSpeedHighGear = 2.5;

    /** Max turn speed of the robot in radians / second */
    public static final double kMaxTurnSpeed = 2.0;

    /** Max speed of the arm in degrees / second */
    public static final double kMaxArmSpeed = 60;

    /** Max arm angle */
    public static final double kMaxArmAngle = 35;
  }

  /** 
   * Constants for Drive
   */
  public static final class DriveConstants {
    // Wheels

    /** Diameter of the wheels in meters */
    public static final double kWheelDiameter = Units.inchesToMeters(5); // Colson 5" Wheels

    /** distance between the wheels of the robot */
    public static final double kTrackWidth = 0.65;
    public static final DifferentialDriveKinematics kDriveKinematics = new DifferentialDriveKinematics(kTrackWidth);

    // Gearing

    /** High Gear 9:1 */
    public static final double kHighGearRatio = (double) 34/12 * 64/20;
    /** Low Gear 33:1 */
    public static final double kLowGearRatio = (double) 50/12 * 60/24 * 64/20;

    // feedforward
    public static final double kDriveBaseVolts = 0.083745;
    public static final double kDriveVoltsSecondsPerMeterHighGear = 2.883;
    public static final double kDriveVoltsSecondsPerMeterLowGear = 10.597;
    public static final double kDriveVoltsSecondsSquaredPerMeterHighGear = 0.57806;
    public static final double kDriveVoltsSecondsSquaredPerMeterLowGear = 0.57806;

    // PID Constants
    public static final double kProportionalCoeff = 4;
    public static final double kIntegralCoeff = 0;
    public static final double kDerivativeCoeff = 0;
  }

  /** 
   * All constants for the arm go here
   */
  public static final class ArmConstants {

    // Physical characteristics
    public static final double kUpperArmLength = 0.80;
    public static final double kLowerArmLength = 0.70;
    // Mass of the arm in Kg
    public static final double kLowerArmMass = 1.9;
    public static final double kLowerArmMaxTorque = 10.4;


    /** the gear ratio between the motor and the arm (50:1) */
    public static final double kGearRatio = (double) 50 / 1;


    // 0 degrees is the arm straight out
    /** the starting angle of the lower arm in degrees */
    public static final double kArmStartingAngle = -90.0;
    /** the inward angle of the upper arm in degrees */
    public static final double kUpperArmInAngle = 90.0;
    /** the forward angle of the upper arm in degrees */
    public static final double kUpperArmOutAngle = 60.0;

    /**
     * Time in seconds to deploy the arm
     */
    public static final double kArmDeployTime = 1;

    // feedforward constants
    public static final double kStatic = 0;
    public static final double kGravity = 1.1; // should be about 1.2 Volts
    // 12 / freeSpeed
    public static final double kVelocity = 0.019; // 12.0 / ( (5676.0 / kGearRatio / 60) * 360)
  }

  public static final class IntakeConstants {
    public static final double kIntakeDeployTime = 1;

    public static final double kCubeIntakeUpperPower = 0.45;
    public static final double kCubeIntakeLowerPower = 0.65;
  }

  public static final class CameraConstants {
    // Distortion constants (For LifeCam by Microsoft)
    // Copied from https://www.chiefdelphi.com/t/wpilib-apriltagdetector-sample-code/421411/21
    public static final double kCX = 345.61;
    public static final double kCY = 207.13;
    public static final double kFX = 699.3778103158814;
    public static final double kFY = 677.716;

    public static final double kAprilTagSize = Units.inchesToMeters(6);
  }
}
