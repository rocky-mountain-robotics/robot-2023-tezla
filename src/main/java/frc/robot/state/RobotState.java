// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.state;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import edu.wpi.first.wpilibj2.command.CommandBase;

/** Class representing the state of the manipulator of the robot */
public class RobotState {

  private static int instances = 0;

  private final int value;

  /** the ways to transform this state into other states */
  private Map<RobotState, CommandBase> transforms;

  /** the distance this state is from other states */
  private Map<RobotState, Integer> distanceTable;
  /** list of states which can reach this one in one step */
  private List<RobotState> linkedStates;

  /**
   * Constructs a new state from a value. Do not create an instance of this
   * class outside of the StateSpace class. It will not contain the connections
   * to other states.
   * 
   * @param value the value of the state
   */
  public RobotState() {
    instances++;
    value = instances;

    transforms = new HashMap<RobotState, CommandBase>();

    distanceTable = new HashMap<RobotState, Integer>();
    distanceTable.put(this, 0);

    linkedStates = new ArrayList<RobotState>();
  }

  /**
   * Adds a link from the current state to another state. THIS DOES NOT UPDATE THE 
   * DISTANCE OF THIS STATE TO THE NEXT STATE. That must be done by the Object 
   * configuring the states.
   * 
   * @param nextState a state which there is a command for changing into 
   * @param transformCommand the command for changing from the current state to the next state
   */
  public void addTransform(RobotState nextState, CommandBase transformCommand) {
    transforms.put(nextState, transformCommand);
    nextState.linkedStates.add(this);

    recursiveUpdateDistances(nextState);
  }

  private void recursiveUpdateDistances(RobotState source) {
    if (updateDistances(source)) {
      for (RobotState state : linkedStates) {
        state.recursiveUpdateDistances(this);
      }
    }
  }

  private boolean updateDistances(RobotState source) {
    boolean hasUpdated = false;

    for (Entry<RobotState, Integer> entry : source.distanceTable.entrySet()) {
      RobotState state = entry.getKey();
      int distance = entry.getValue();

      if (!distanceTable.containsKey(state)) {
        distanceTable.put(state, distance + 1);
        hasUpdated = true;
      }

      if (distance + 1 < distanceTable.get(state)) {
        distanceTable.replace(state, distance + 1);
        hasUpdated = true;
      }
    }

    return hasUpdated;
  }

  public CommandBase getCommand(RobotState finalState) {
    return transforms.get(getNextState(finalState));
  }

  /**
   * 
   * @param finalState the target state
   * @return the next closest state
   */
  public RobotState getNextState(RobotState finalState) {
    if (!canReachState(finalState)) {
      throw new IllegalArgumentException("cannot reach final state");
    }

    // already at the final state
    if (equals(finalState)) {
      return this;
    }

    int distance = distanceTable.get(finalState);

    for (RobotState state: transforms.keySet()) {
      if (state.canReachState(finalState)) {
        // check if this is the next state
        if (state.distanceTo(finalState) < distance) {
          return state;
        }
      }
    }

    // should not be able to reach this point
    return this;
  }

  public int distanceTo(RobotState state) {
    return distanceTable.get(state);
  }

  public boolean canReachState(RobotState state) {
    return distanceTable.containsKey(state);
  }

  @Override
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    }
    if (o == null || o.getClass() != getClass()) {
      return false;
    }

    RobotState state = (RobotState) o;
    return state.value == this.value;
  }

  @Override
  public int hashCode() {
    return value;
  }
}