// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.state;

import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.InstantCommand;

public class TransformStateCommand extends CommandBase {

  private RobotStateGraph states;

  private RobotState m_targetState;

  private CommandBase m_currentCommand;
  private RobotState m_currentState;

  @SuppressWarnings("unused")
  private RobotState m_nextState;

  /** Creates a new TransformStateCommand. */
  public TransformStateCommand(RobotStateGraph robotStates, RobotState finalState) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(robotStates.getRequirements());

    m_targetState = finalState;
    states = robotStates;

    // determine initial state
    m_currentState = states.getCurrentState();

    if (m_currentState.canReachState(m_targetState) && !m_currentState.equals(m_targetState)) {
      m_nextState = m_currentState.getNextState(m_targetState);
      m_currentCommand = m_currentState.getCommand(m_targetState);
    }
    else {
      // do nothing
      m_currentCommand = new InstantCommand();
    }
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    // determine initial state
    m_currentState = states.getCurrentState();

    if (m_currentState.canReachState(m_targetState) && !m_currentState.equals(m_targetState)) {
      m_nextState = m_currentState.getNextState(m_targetState);
      m_currentCommand = m_currentState.getCommand(m_targetState);
    }
    else {
      // do nothing
      m_currentCommand = new InstantCommand();
    }

    // initialize the current command
    m_currentCommand.initialize();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    m_currentCommand.execute();

    if (m_currentCommand.isFinished()) {
      m_currentCommand.end(false);

      m_currentState = states.getCurrentState();

      // done
      if (m_currentState.equals(m_targetState)) {
        m_currentCommand = new InstantCommand();
        return;
      }

      // cannot do anything
      if (!m_currentState.canReachState(m_currentState)) {
        m_currentCommand = new InstantCommand();
        return;
      }

      m_nextState = m_currentState.getNextState(m_targetState);
      // get next command
      m_currentCommand = m_currentState.getCommand(m_targetState);
      m_currentCommand.initialize();
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    m_currentCommand.end(interrupted);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    if (!m_currentState.canReachState(m_targetState)) {
      return true;
    }

    // at target state
    if (m_currentState.equals(m_targetState)) {
      return true;
    }

    return false;
  }
}
