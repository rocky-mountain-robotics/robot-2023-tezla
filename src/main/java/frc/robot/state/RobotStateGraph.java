// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.state;

import edu.wpi.first.wpilibj2.command.Commands;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.commands.ArmSetPointCommand;
import frc.robot.commands.DeployIntakeCommand;
import frc.robot.commands.DeployShoulderCommand;
import frc.robot.commands.RetractIntakeCommand;
import frc.robot.commands.RetractShoulderCommand;
import frc.robot.subsystems.ArmSubsystem;
import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.ShoulderSubsystem;

/** Add your docs here. */
public class RobotStateGraph {

  private ShoulderSubsystem shoulder;
  private ArmSubsystem arm;
  private IntakeSubsystem intake;

  // TODO: write getters
  public final RobotState initialState = new RobotState();
  public final RobotState intakeState = new RobotState();
  public final RobotState collectState = new RobotState();
  public final RobotState lowGoalState = new RobotState();
  public final RobotState highGoalState = new RobotState();
  public final RobotState goalTransferState = new RobotState();

  public final RobotState unknownState = new RobotState();

  public RobotStateGraph(ShoulderSubsystem robotShoulder, ArmSubsystem robotArm, IntakeSubsystem robotIntake) {

    shoulder = robotShoulder;
    arm = robotArm;
    intake = robotIntake;

    // set up graph
    initialState.addTransform(initialState, new DeployIntakeCommand(robotIntake));

    intakeState.addTransform(initialState, Commands.sequence(
      new ArmSetPointCommand(robotArm, -90),
      new RetractIntakeCommand(robotIntake)));
    initialState.addTransform(collectState, Commands.sequence(
      new ArmSetPointCommand(robotArm, -90),
      new DeployShoulderCommand(robotShoulder)));
    intakeState.addTransform(lowGoalState, Commands.sequence(
      new ArmSetPointCommand(robotArm, 0),
      new RetractIntakeCommand(robotIntake)));
    intakeState.addTransform(goalTransferState, Commands.sequence(
      new ArmSetPointCommand(robotArm, 1),
      new DeployShoulderCommand(robotShoulder)));
    
    collectState.addTransform(intakeState, new RetractShoulderCommand(robotShoulder));
    
    lowGoalState.addTransform(intakeState, Commands.sequence(
      new ArmSetPointCommand(robotArm, 0),
      new DeployIntakeCommand(robotIntake)));
    lowGoalState.addTransform(highGoalState, Commands.sequence(
      new ArmSetPointCommand(robotArm, 1),
      new DeployShoulderCommand(robotShoulder)));

    highGoalState.addTransform(lowGoalState, Commands.sequence(
      new ArmSetPointCommand(robotArm, -31),
      new RetractShoulderCommand(robotShoulder)));
    highGoalState.addTransform(goalTransferState, Commands.sequence(
      new ArmSetPointCommand(robotArm, 0),
      new DeployIntakeCommand(robotIntake)));

    goalTransferState.addTransform(intakeState, Commands.sequence(
      new ArmSetPointCommand(robotArm, -31),
      new RetractShoulderCommand(robotShoulder)));
  }

  public SubsystemBase[] getRequirements() {
    return new SubsystemBase[] {shoulder, arm, intake};
  }

  /**
   * 
   * @return the state the robot is currently in
   */
  public RobotState getCurrentState() {

    if (!intake.isDeployed() && !intake.isRetracted()) {
      return unknownState;
    }
    if (!shoulder.isShoulderIn() && !shoulder.isShoulderOut()) {
      return unknownState;
    }

    boolean armIn = false;

    if (shoulder.isShoulderIn()) {
      armIn = arm.getLowerArmPosition() < -85;
    }
    else if (shoulder.isShoulderOut()) {
      armIn = arm.getLowerArmPosition() < -105;
    }


    return getState(
      intake.isRetracted(), 
      shoulder.isShoulderIn(), 
      armIn);
  }

  /**
   * 
   * @param intakeIn whether the intake is retracted
   * @param shoulderIn whether the shoulder is in the upright position
   * @param armIn whether the arm is behind the intake
   * @return the current robot state
   */
  public RobotState getState(boolean intakeIn, boolean shoulderIn, boolean armIn) {
    int value = 
      (intakeIn ? 0b100 : 0b000) | 
      (shoulderIn ? 0b010 : 0b000) |
      (armIn ? 0b001 : 0b000);
    
    switch (value) {
      // everything in
      case 0b111:
        return initialState;
      
      // intake deployed, shoulder in
      case 0b011:
      case 0b010:
        return intakeState;
      
      // intake deployed, shoulder out, arm in
      case 0b001:
        return collectState;

      // intake in, shoulder in, arm out
      case 0b110:
        return lowGoalState;
      
      // intake in, shoulder out, arm out
      case 0b100:
        return highGoalState;
      
      // intake deployed, shoulder out, arm out
      case 0b000:
        return goalTransferState;

      // case 0b010
      default:
        //throw new IllegalArgumentException("illegal state");
    }

    return intakeState;
  } 
}
