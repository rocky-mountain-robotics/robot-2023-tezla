// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.GripperSubsystem;

public class ToggleGripperCommand extends CommandBase {
  GripperSubsystem robotGripper;

  /** Creates a new ToggleGripperCommand. */
  public ToggleGripperCommand(GripperSubsystem gripper) {
    // Use addRequirements() here to declare subsystem dependencies.
    robotGripper = gripper;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    robotGripper.closeGripper();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {}

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    robotGripper.openGripper();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
