// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.math.controller.ArmFeedforward;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants.Control;
import frc.robot.subsystems.ArmSubsystem;
import static frc.robot.Constants.OperatorConstants.*;

public class TeleOpArmElbowCommand extends CommandBase {
  private ArmSubsystem armSubsystem;

  private ArmFeedforward feedforward;
  private PIDController pid;

  private SlewRateLimiter yLimiter;

  private Joystick manipJoystick = new Joystick(kManipulatorControllerPort);

  /** Creates a new TeleOpArmElbowCommand. */
  public TeleOpArmElbowCommand(ArmSubsystem arm) {
    armSubsystem = arm;
    addRequirements(armSubsystem);

    feedforward = armSubsystem.getFeedforward();
    pid = new PIDController(0.6 / 60.0, 0, 0);

    yLimiter = new SlewRateLimiter(100);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    // positive is up
    double armInput = withDeadZone(-manipJoystick.getY(), 0.2);
    double armSetpoint = yLimiter.calculate(armInput * Control.kMaxArmSpeed);

    // at its lowest postion
    if (armSubsystem.limitSignalReceived()) {
      if (armSetpoint < 0) {
        armSetpoint = 0;
      }
    }

    // at its highest postion
    else if (armSubsystem.getLowerArmPosition() > Control.kMaxArmAngle) {
      if (armSetpoint > 0) {
        armSetpoint = 0;
      }
    }

    double feedforwardVoltage = feedforward.calculate(
      Units.degreesToRadians(armSubsystem.getLowerArmPosition()), 
      armSetpoint);

    double pidVoltage = pid.calculate(armSubsystem.getLowerArmVelocity(), armSetpoint);

    // command subsystem
    armSubsystem.setVoltage(
      feedforwardVoltage + 
      pidVoltage
    );
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    armSubsystem.setVoltage(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }

  private double withDeadZone(double value, double deadZone) {
    if (value > deadZone) {
      return (value - deadZone) / (1 - deadZone);
    }
    else if (value < -deadZone) {
      return (value + deadZone) / (1 - deadZone);
    }

    return 0;
  }
}
