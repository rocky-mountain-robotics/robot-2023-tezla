// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

/*
 * Editors:
 * Caleb Borlin (Lead)
 */

package frc.robot.commands;


import java.util.List;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrajectoryConfig;
import edu.wpi.first.math.trajectory.TrajectoryGenerator;
import edu.wpi.first.math.trajectory.constraint.DifferentialDriveVoltageConstraint;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.Commands;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.Constants.AutoConstants;
import frc.robot.Constants.DriveConstants;
import frc.robot.components.Gyroscope;
import frc.robot.subsystems.ArmSubsystem;
import frc.robot.subsystems.DriveTrainSubsystem;
import frc.robot.subsystems.GripperSubsystem;
import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.ShoulderSubsystem;

//Setting "guidlines" for autonomous code
public final class Autos {
  /** Example static factory for an autonomous command. */
  public static CommandBase exampleAuto(DriveTrainSubsystem robotDrive) {

    // Create a voltage constraint to ensure we don't accelerate too fast
    var autoVoltageConstraint =
      new DifferentialDriveVoltageConstraint(
        robotDrive.getLowGearFeedforward(),
        DriveConstants.kDriveKinematics,
        10);

    // Create config for trajectory
    TrajectoryConfig config =
        new TrajectoryConfig(
                AutoConstants.kMaxSpeedMetersPerSecond,
                AutoConstants.kMaxAccelerationMetersPerSecondSquared)
            // Add kinematics to ensure max speed is actually obeyed
            .setKinematics(DriveConstants.kDriveKinematics)
            // Apply the voltage constraint
            .addConstraint(autoVoltageConstraint);


    // An example trajectory to follow.  All units in meters.
    Trajectory exampleTrajectory =
        TrajectoryGenerator.generateTrajectory(
            // Start at the origin facing the +X direction
            new Pose2d(0, 0, new Rotation2d(0)),
            // Pass through these two interior waypoints, making an 's' curve path
            List.of(new Translation2d(1, 1), new Translation2d(2, -1)),
            // End 3 meters straight ahead of where we started, facing forward
            new Pose2d(3, 0, new Rotation2d(0)),
            // Pass config
            config);
    
    Command trajectoryCommand = new FollowTrajectoryCommand(robotDrive, exampleTrajectory);

    return trajectoryCommand.andThen(() -> robotDrive.tankDriveVolts(0, 0));
  }

  public static CommandBase driveForwardAuto(DriveTrainSubsystem robotDrive) {
    // An example trajectory to follow.  All units in meters.
    Trajectory forwardTrajectory =
        TrajectoryGenerator.generateTrajectory(
            // Start at the origin facing the +X direction
            List.of(
              new Pose2d(0, 0, new Rotation2d(0)),
              new Pose2d(3, 0, new Rotation2d(0))),
            // Pass config
            lowGearConfig(robotDrive));
    
    CommandBase trajectoryCommand = new FollowTrajectoryCommand(robotDrive, forwardTrajectory);
    return trajectoryCommand;
  }

  /**
   * Steps:
   * close the gripper
   * deploy intake
   * raise arm
   * deploy shoulder
   * reset arm position
   * drive forward
   * open gripper
   * 
   * @return
   */
  public static CommandBase scoreConeAuto(
      DriveTrainSubsystem robotDrive, 
      ArmSubsystem robotArm, 
      ShoulderSubsystem robotShoulder, 
      IntakeSubsystem robotIntake,
      GripperSubsystem robotGripper) {

    Trajectory setupTrajectory =
      TrajectoryGenerator.generateTrajectory(
          // Start at the origin facing the +X direction
          List.of(
            new Pose2d(0, 0, new Rotation2d(0)),
            new Pose2d(-0.8, 0, new Rotation2d(0))),
          // Pass config
          lowGearConfig(robotDrive, true));

    // An example trajectory to follow.  All units in meters.
    Trajectory scoreTrajectory =
        TrajectoryGenerator.generateTrajectory(
            // Start at the origin facing the +X direction
            List.of(
              new Pose2d(0, 0, new Rotation2d(0)),
              new Pose2d(0.69, 0, new Rotation2d(0))),
            // Pass config
            lowGearConfig(robotDrive));
    
    Trajectory backwardTrajectory =
        TrajectoryGenerator.generateTrajectory(
            // Start at the origin facing the +X direction
            List.of(
              new Pose2d(0, 0, new Rotation2d(0)),
              new Pose2d(-4, 0, new Rotation2d(0))),
            // Pass config
            lowGearConfig(robotDrive, true));
    
    return Commands.sequence(
      new InstantCommand(robotGripper::closeGripper, robotGripper),
      new FollowTrajectoryCommand(robotDrive, setupTrajectory),
      new DeployManipCommandGroup(robotIntake, robotArm, robotShoulder),
      new ArmSetPointCommand(robotArm, 33),
      new FollowTrajectoryCommand(robotDrive, scoreTrajectory),
      new WaitCommand(0.5),
      new InstantCommand(robotGripper::openGripper, robotGripper),
      new WaitCommand(0.2),
      new FollowTrajectoryCommand(robotDrive, backwardTrajectory)
    );
  }

  /**
   * Steps:
   * close the gripper
   * deploy intake
   * raise arm
   * deploy shoulder
   * reset arm position
   * drive forward
   * open gripper
   * 
   * @return
   */
  public static CommandBase scoreCubeAuto(
      DriveTrainSubsystem robotDrive, 
      ArmSubsystem robotArm, 
      ShoulderSubsystem robotShoulder, 
      IntakeSubsystem robotIntake,
      GripperSubsystem robotGripper) {

    Trajectory setupTrajectory =
      TrajectoryGenerator.generateTrajectory(
          // Start at the origin facing the +X direction
          List.of(
            new Pose2d(0, 0, new Rotation2d(0)),
            new Pose2d(-0.4, 0, new Rotation2d(0))),
          // Pass config
          lowGearConfig(robotDrive, true));

    // An example trajectory to follow.  All units in meters.
    Trajectory scoreTrajectory =
        TrajectoryGenerator.generateTrajectory(
            // Start at the origin facing the +X direction
            List.of(
              new Pose2d(0, 0, new Rotation2d(0)),
              new Pose2d(0.29, 0, new Rotation2d(0))),
            // Pass config
            lowGearConfig(robotDrive));
    
    Trajectory backwardTrajectory =
        TrajectoryGenerator.generateTrajectory(
            // Start at the origin facing the +X direction
            List.of(
              new Pose2d(0, 0, new Rotation2d(0)),
              new Pose2d(-0.8, 0.3, new Rotation2d(0))
              //new Pose2d(-4, 0.2, new Rotation2d(0))
            ),
            // Pass config
            lowGearConfig(robotDrive, true));
    
    return Commands.sequence(
      new InstantCommand(robotGripper::closeGripper, robotGripper),
      new FollowTrajectoryCommand(robotDrive, setupTrajectory),
      new DeployManipCommandGroup(robotIntake, robotArm, robotShoulder),
      new ArmSetPointCommand(robotArm, 15),
      new FollowTrajectoryCommand(robotDrive, scoreTrajectory),
      new WaitCommand(0.5),
      new InstantCommand(robotGripper::openGripper, robotGripper),
      new WaitCommand(0.2),
      new FollowTrajectoryCommand(robotDrive, setupTrajectory)
    );
  }

  public static CommandBase scoreConeIntakeCubeAuto(DriveTrainSubsystem robotDrive, ArmSubsystem robotArm, ShoulderSubsystem robotShoulder, IntakeSubsystem robotIntake, GripperSubsystem robotGripper) {
    Trajectory scoreTrajectory =
        TrajectoryGenerator.generateTrajectory(
            // Start at the origin facing the +X direction
            List.of(
              new Pose2d(0, 0, new Rotation2d(0)),
              new Pose2d(0.3, 0, new Rotation2d(0))),
            // Pass config
            lowGearConfig(robotDrive));

    Trajectory retractTrajectory =
      TrajectoryGenerator.generateTrajectory(
          // Start at the origin facing the +X direction
          List.of(
            new Pose2d(0, 0, new Rotation2d(0)),
            new Pose2d(-6, 0, new Rotation2d(0))),
          // Pass config
          lowGearConfig(robotDrive));

    Trajectory intakeTrajectory =
      TrajectoryGenerator.generateTrajectory(
        // Start at the origin facing the +X direction
        List.of(
          new Pose2d(0, 0, new Rotation2d(0)),
          new Pose2d(1, -0.5, new Rotation2d(0))),
        // Pass config
        lowGearConfig(robotDrive));
    
    // TODO: drive forward measure actual distance
    return Commands.sequence(
      new InstantCommand(robotGripper::closeGripper, robotGripper),
      new DeployManipCommandGroup(robotIntake, robotArm, robotShoulder),
      new ArmSetPointCommand(robotArm, 30),
      new FollowTrajectoryCommand(robotDrive, scoreTrajectory),
      new WaitCommand(2.0),
      new InstantCommand(robotGripper::openGripper, robotGripper),
      new WaitCommand(1),
      Commands.parallel(
        new RetractManipCommandGroup(robotIntake, robotArm, robotShoulder), 
        new FollowTrajectoryCommand(robotDrive, retractTrajectory)
      ),
      Commands.parallel(
        new FollowTrajectoryCommand(robotDrive, intakeTrajectory),
        new BlackHolenateCommand(robotIntake).withTimeout(3)
      ));
  }

  public static CommandBase balanceAuto(DriveTrainSubsystem robotDrive, IntakeSubsystem robotIntake, GripperSubsystem robotGripper, Gyroscope robotGyro) {
    // An example trajectory to follow.  All units in meters.
    Trajectory balanceTrajectory =
        TrajectoryGenerator.generateTrajectory(
            // Start at the origin facing the +X direction
            List.of(
              new Pose2d(0, 0, new Rotation2d(0)),
              new Pose2d(1.6, 0, new Rotation2d(0))),
            // Pass config
            lowGearConfig(robotDrive));
    
    // drive forwards 1 meter
    return Commands.sequence(
      new InstantCommand(robotGripper::closeGripper, robotGripper),
      new WaitCommand(1.0),
      new DeployIntakeCommand(robotIntake),
      new FollowTrajectoryCommand(robotDrive, balanceTrajectory),
      new AutoBalanceCommand(robotDrive, robotGyro)
    );
  }

  public static CommandBase scoreBalanceAuto(DriveTrainSubsystem robotDrive, IntakeSubsystem robotIntake, Gyroscope robotGyro) {

    Trajectory balanceTrajectory =
        TrajectoryGenerator.generateTrajectory(
            // Start at the origin facing the +X direction
            List.of(
              new Pose2d(0, 0, new Rotation2d(0)),
              new Pose2d(-1.6, 0, new Rotation2d(0))),
            // Pass config
            lowGearConfig(robotDrive));
    
    // spin the intake while its not deployed to shoot a cube and then balance
    return Commands.sequence(
      new WhiteHolenateCommand(robotIntake).withTimeout(0.5),
      new FollowTrajectoryCommand(robotDrive, balanceTrajectory),
      new AutoBalanceCommand(robotDrive, robotGyro)
    );
  }

  private static TrajectoryConfig lowGearConfig(DriveTrainSubsystem robotDrive) {
    return lowGearConfig(robotDrive, false);
  }

  private static TrajectoryConfig lowGearConfig(DriveTrainSubsystem robotDrive, boolean reversed) {
    DifferentialDriveVoltageConstraint autoVoltageConstraint =
      new DifferentialDriveVoltageConstraint(
        robotDrive.getLowGearFeedforward(),
        DriveConstants.kDriveKinematics,
        10);

    // Create config for trajectory
    TrajectoryConfig config =
        new TrajectoryConfig(
                AutoConstants.kMaxSpeedMetersPerSecond,
                AutoConstants.kMaxAccelerationMetersPerSecondSquared)
            // Add kinematics to ensure max speed is actually obeyed
            .setKinematics(DriveConstants.kDriveKinematics)
            // Apply the voltage constraint
            .addConstraint(autoVoltageConstraint);
    
    config.setReversed(reversed);

    return config;
  }

  private Autos() {
    throw new UnsupportedOperationException("This is a utility class!");
  }
}
