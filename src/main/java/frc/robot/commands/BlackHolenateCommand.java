// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.networktables.TunedDouble;
import frc.robot.subsystems.IntakeSubsystem;

public class BlackHolenateCommand extends CommandBase {
  private IntakeSubsystem intakeSubsystem;
  private TunedDouble maxUpperPower;
  private TunedDouble maxLowerPower;

  /** Creates a new BlackHolenateCommand. */
  public BlackHolenateCommand(IntakeSubsystem intake) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(intake);

    intakeSubsystem = intake;

    maxUpperPower = new TunedDouble("Intake Upper Max Power", 0.5);

    maxLowerPower = new TunedDouble("Intake Lower Max Power", 0.7);

  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    intakeSubsystem.blackHole(maxUpperPower.getValue(), maxLowerPower.getValue());
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    intakeSubsystem.blackHole(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
