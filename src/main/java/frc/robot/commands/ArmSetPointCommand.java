// Copyright (c) FIRST and other WPILib contributors.
// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.trajectory.TrapezoidProfile;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ArmSubsystem;

public class ArmSetPointCommand extends CommandBase {
  private ArmSubsystem elbowArm;

  private final double kDt = 0.02;

  private TrapezoidProfile.Constraints constraints;
  private TrapezoidProfile.State goal;
  private TrapezoidProfile.State setpoint;

  public double currentPosition;

  private PIDController pidController;

  private double angle;
  
  /** Creates a new DeployElbowCommand. */
  public ArmSetPointCommand(ArmSubsystem arm, double angle) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(arm);

    this.angle = angle;

    elbowArm = arm;

    setpoint = new TrapezoidProfile.State();

    constraints = new TrapezoidProfile.Constraints(90, 180);
    
    goal = new TrapezoidProfile.State(angle, 0);

    pidController = new PIDController(0.05, 0.25, 0.01);
    pidController.setIntegratorRange(-0.3, 0.3);
    SmartDashboard.putData("Arm Setpoint PID", pidController);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    setpoint = new TrapezoidProfile.State(elbowArm.getLowerArmPosition(), elbowArm.getLowerArmVelocity());
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {

    currentPosition = elbowArm.getLowerArmPosition();

    TrapezoidProfile profile = new TrapezoidProfile(constraints, goal, setpoint);

    setpoint = profile.calculate(kDt);

    double pidControllerVoltage = pidController.calculate(elbowArm.getLowerArmPosition(), setpoint.position);

    double feedfowardVoltage = elbowArm.getFeedforward()
      .calculate(Units.degreesToRadians(elbowArm.getLowerArmPosition()), setpoint.velocity);

    elbowArm.setVoltage(pidControllerVoltage + feedfowardVoltage);

    SmartDashboard.putNumber("setpoint position", setpoint.position);
    SmartDashboard.putNumber("actual position", currentPosition);

    SmartDashboard.putNumber("setpoint velocity", setpoint.velocity);
    SmartDashboard.putNumber("actual velocity", elbowArm.getLowerArmVelocity());

    SmartDashboard.putNumber("position error", setpoint.position - currentPosition);
    SmartDashboard.putNumber("velocity error", setpoint.velocity - elbowArm.getLowerArmVelocity());
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    // TODO: Use arm hold command to keep arm level

    elbowArm.setVoltage(elbowArm.getFeedforward().calculate(Units.degreesToRadians(elbowArm.getLowerArmPosition()), 0));
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    // TODO: end on a timer
    SmartDashboard.putNumber("error left", (currentPosition - angle));
    return Math.abs(currentPosition - angle) < 8;
    // return (89.0 < currentPosition) && (currentPosition < 91.0);
  }
}
