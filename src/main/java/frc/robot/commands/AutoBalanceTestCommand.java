// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

/*
 * Derek Pendley (Lead)
 */

package frc.robot.commands;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.components.Gyroscope;
import frc.robot.subsystems.DriveTrainSubsystem;
import frc.robot.Constants.Control;
import frc.robot.Constants.DriveConstants;

public class AutoBalanceTestCommand extends CommandBase {
  private NetworkTable balanceTables;
  private NetworkTableEntry motorVoltageEntry;

  private DriveTrainSubsystem balanceDrive;
  
  private Gyroscope gyroscope;

  private PIDController pid;
  private SimpleMotorFeedforward feedforward;

  /** Creates a new AutoBalanceCommand. */
  public AutoBalanceTestCommand(DriveTrainSubsystem robotDrive, Gyroscope robotGyro) {
    // Use addRequirements() here to declare subsystem dependencies.
    //addRequirements(robotDrive);
    balanceTables = NetworkTableInstance.getDefault().getTable("Balance Tables");
    motorVoltageEntry = balanceTables.getEntry("Motor Voltage");

    balanceDrive = robotDrive;

    gyroscope = robotGyro;

    pid = new PIDController(DriveConstants.kProportionalCoeff, 0, DriveConstants.kDerivativeCoeff);
    feedforward = robotDrive.getLowGearFeedforward();
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    // balanceDrive.setBreakMode();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {

    double angle = gyroscope.getBalanceAngle();

  //Charging Station only has two angles 11 degrees for ramp and 15 degrees for completely tilted    
    double speed = 0;
    //when tilted forwards, angle > 0
    if(angle > 9){
      speed = -0.125;
    }
    //when tilted backwards, angle < 0
    else if(angle < -9){
      speed = 0.125;
    }
    else{
      speed = 0;
    }
    
    double motorVoltage = feedforward.calculate(speed);
    double currentVelocity = (balanceDrive.getLeftVelocity() + balanceDrive.getRightVelocity()) / 2;
    motorVoltage += pid.calculate(currentVelocity, speed);
    
    motorVoltageEntry.setDouble(motorVoltage);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    // balanceDrive.setCoastMode();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}