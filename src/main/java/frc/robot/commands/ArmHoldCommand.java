// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ArmSubsystem;

public class ArmHoldCommand extends CommandBase {
  private ArmSubsystem armSubsystem;
  private double armSetPoint;
  private double velocitySetpoint;


  public double currentPosition;

  private PIDController pidController;
  /** Creates a new TeleOpArmShoulderCommand. */
  public ArmHoldCommand(ArmSubsystem robotArm) {
    addRequirements(robotArm);

    armSubsystem = robotArm;
    pidController = new PIDController(0.05, 0, 0.01);
  }
  

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    armSetPoint = armSubsystem.getLowerArmPosition(); 
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    currentPosition = armSubsystem.getLowerArmPosition();

    double pidControllerVoltage = pidController.calculate(armSubsystem.getLowerArmPosition(), armSetPoint);
    velocitySetpoint = pidController.getVelocityError();

    double feedfowardVoltage = armSubsystem.getFeedforward()
      .calculate(Units.degreesToRadians(armSubsystem.getLowerArmPosition()), velocitySetpoint);
    
    armSubsystem.setVoltage(pidControllerVoltage + feedfowardVoltage);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    armSubsystem.setVoltage(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
