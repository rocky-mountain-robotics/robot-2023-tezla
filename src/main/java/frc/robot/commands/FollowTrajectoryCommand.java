// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.RamseteController;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.wpilibj2.command.RamseteCommand;
import frc.robot.Constants.AutoConstants;
import frc.robot.Constants.Control;
import frc.robot.Constants.DriveConstants;
import frc.robot.subsystems.DriveTrainSubsystem;

public class FollowTrajectoryCommand extends RamseteCommand { 
  // TODO: Reimplement Ramsete Controller

  /** Creates a new FollowTrajectoryCommand. */
  public FollowTrajectoryCommand(DriveTrainSubsystem robotDrive, Trajectory trajectory) {
    super(
      trajectory,
      robotDrive::getPose,
      getRamseteController(),
      robotDrive.getLowGearFeedforward(),
      DriveConstants.kDriveKinematics,
      robotDrive::getWheelSpeeds,
      new PIDController(AutoConstants.kDriveKp, 0, 0),
      new PIDController(AutoConstants.kDriveKp, 0, 0),
      // RamseteCommand passes volts to the callback
      robotDrive::tankDriveVolts,
      robotDrive);
    
    robotDrive.setLowGear();
    robotDrive.resetOdometry(trajectory.getInitialPose());
    robotDrive.getField().getObject("trajectory").setTrajectory(trajectory);
  }

  private static RamseteController getRamseteController() {
    RamseteController controller = new RamseteController(AutoConstants.kRamseteB, AutoConstants.kRamseteZeta);
    controller.setEnabled(true);
    return controller;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    super.initialize();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    super.execute();
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    super.end(interrupted);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return super.isFinished();
  }
}
