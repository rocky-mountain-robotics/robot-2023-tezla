// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

/*
 * Editors:
 * Caleb Borlin (Lead)
 * 
 * Josh Dreesman
 * Wyatt White
 * Drew Schloesser
 */

package frc.robot.commands;

import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveTrainSubsystem;

import static frc.robot.Constants.OperatorConstants.*;
import static frc.robot.Constants.DriveConstants.*;
import static frc.robot.Constants.Control.*;

public class TeleOpDriveCommand extends CommandBase {

  private DriveTrainSubsystem drive;

  private SlewRateLimiter forwardLimiter;
  private SlewRateLimiter turnLimiter;

  private Joystick driveJoystick = new Joystick(kDriverControllerPort);


  /** Creates a new TeleOpDriveCommand. */
  public TeleOpDriveCommand(DriveTrainSubsystem robotDrive) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(robotDrive);

    drive = robotDrive;

    forwardLimiter = new SlewRateLimiter(3.0);
    turnLimiter = new SlewRateLimiter(6.0);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {

    double forwardInput = -withDeadZone(driveJoystick.getY(), 0.2);
    double turnInput = -withDeadZone(driveJoystick.getTwist(), 0.2);

    ChassisSpeeds chassisSpeeds = new ChassisSpeeds();

    if (drive.inLowGear()) {
      chassisSpeeds = new ChassisSpeeds(
        forwardLimiter.calculate(forwardInput * kMaxSpeedLowGear), 
        0,
        turnLimiter.calculate(turnInput * kMaxTurnSpeed)
      );
    }

    if (drive.inHighGear()) {
      chassisSpeeds = new ChassisSpeeds(
        forwardLimiter.calculate(forwardInput * kMaxSpeedHighGear), 
        0,
        turnLimiter.calculate(turnInput * kMaxTurnSpeed)
      );
    }

    drive.setWheelSpeeds(kDriveKinematics.toWheelSpeeds(chassisSpeeds));

  }


  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }

  private double withDeadZone(double value, double deadZone) {
    if (value > deadZone) {
      return (value - deadZone) / (1 - deadZone);
    }
    else if (value < -deadZone) {
      return (value + deadZone) / (1 - deadZone);
    }

    return 0;
  }
}
