// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Commands;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.ArmSubsystem;
import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.ShoulderSubsystem;

public class RetractManipCommandGroup extends SequentialCommandGroup {

  /** Creates a new RetractManipCommandGroup. */
  public RetractManipCommandGroup(IntakeSubsystem intake, ArmSubsystem arm, ShoulderSubsystem shoulder) {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      new DeployIntakeCommand(intake),
      Commands.deadline(
        new RetractShoulderCommand(shoulder)
        //new ArmHoldCommand(arm)
      ),
      new ArmSetPointCommand(arm, -90),
      new RetractIntakeCommand(intake)
    );
  }
}
