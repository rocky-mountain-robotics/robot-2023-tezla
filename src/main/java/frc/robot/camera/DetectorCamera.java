// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.camera;


import edu.wpi.first.apriltag.AprilTagDetection;
import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.cscore.CvSink;
import edu.wpi.first.cscore.CvSource;
import edu.wpi.first.cscore.UsbCamera;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import edu.wpi.first.vision.VisionThread;

/**
 * Here is the Source 
 * @link https://www.chiefdelphi.com/t/wpilib-apriltagdetector-sample-code/421411/30
 * 
 * This is the class for camera detections. All camera code runs in the background.
 */
public class DetectorCamera {

  private final CvSink m_cvSink = new CvSink("Detector Camera CvSink");

  private Thread visionThread;
  private UsbCamera camera;
  private CvSource outputStream;

  private final Mat mat = new Mat();

  public DetectorCamera() {

    AprilTagPipeline pipeline = new AprilTagPipeline();
    
    camera = CameraServer.startAutomaticCapture(); // Get the UsbCamera from CameraServer
    camera.setResolution(640, 480); // Set the resolution
    m_cvSink.setSource(camera); // Get a CvSink. This will capture Mats from the camera

    // check if camera is working
    outputStream = CameraServer.putVideo("Detector Camera", 640, 480); // Setup a CvSource. This will send images back to the Dashboard
    visionThread = new VisionThread(camera, pipeline, this::listener); // Creates vision thread using 'listener'
    visionThread.start();
  }

  public void listener(AprilTagPipeline pipeline) {
    m_cvSink.grabFrame(mat);

    AprilTagDetection[] detections = pipeline.getAprilTags();

    for (AprilTagDetection detection : detections) {

        Imgproc.line(mat,
        new Point(detection.getCornerX(0), detection.getCornerY(0)), 
        new Point(detection.getCornerX(1), detection.getCornerY(1)),
        new Scalar(0, 255, 0), 5);

        Imgproc.line(mat,
        new Point(detection.getCornerX(1), detection.getCornerY(1)), 
        new Point(detection.getCornerX(2), detection.getCornerY(2)),
        new Scalar(0, 255, 0), 5);

        Imgproc.line(mat,
        new Point(detection.getCornerX(2), detection.getCornerY(2)), 
        new Point(detection.getCornerX(3), detection.getCornerY(3)),
        new Scalar(0, 255, 0), 5);

        Imgproc.line(mat,
        new Point(detection.getCornerX(3), detection.getCornerY(3)), 
        new Point(detection.getCornerX(0), detection.getCornerY(0)),
        new Scalar(0, 255, 0), 5);

        // Shows the ID for the AprilTag in the center of the tag, in blue
        Imgproc.putText(mat,
        String.valueOf(detection.getId()),
        new Point(detection.getCenterX(), detection.getCenterY()),
        5,
        3,
        new Scalar(0, 0, 255), 5);
    }
    

    // Give the output stream a new image to display
    outputStream.putFrame(mat);
  }


}
