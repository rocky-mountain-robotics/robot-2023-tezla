// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.camera;
import org.opencv.core.Mat;
import edu.wpi.first.vision.VisionPipeline;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.apriltag.AprilTagDetection;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Pose3d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Rotation3d;
import edu.wpi.first.math.geometry.Transform3d;
import edu.wpi.first.math.geometry.Translation3d;
import edu.wpi.first.math.util.Units;

/**
 * Pipeline for finding april tags from a Life Cam.
 * 
 * <p> Retrieves both the name of the April Tag and the relative position of the
 * Camera compared to the April Tag
 */
public class AprilTagPipeline implements VisionPipeline {

  private AprilTagProcessing processor;
  private AprilTagDetection[] detections;
  private Transform3d[] locations;

  private Pose2d cameraPosition;

  private Field2d cameraField;

  public AprilTagPipeline(){
    processor = new AprilTagProcessing();
    detections = new AprilTagDetection[0];
    locations = new Transform3d[0];

    cameraField = new Field2d();
    SmartDashboard.putData(cameraField);
  }

  @Override
  public void process(Mat image) {
    detections = processor.detectAprilTags(image);
    locations = processor.processAprilTags(detections);

    Transform3d location = null;
    if(locations.length > 0){
      location = locations[0];
    }

    if(location != null){
      Transform3d cameraPose3d = new Transform3d(new Translation3d(), new Rotation3d(0, Units.degreesToRadians(30), 0));
      Pose3d inversePose3d = new Pose3d().transformBy(location.inverse()).transformBy(cameraPose3d.inverse());

      cameraPosition = new Pose2d(inversePose3d.getTranslation().getZ(), inversePose3d.getTranslation().getX(), Rotation2d.fromRadians(inversePose3d.getRotation().getY()));

      cameraField.getObject("Camera").setPose(cameraPosition);
    }


  }

  public AprilTagDetection[] getAprilTags(){
    return detections;
  }

  public Transform3d[] getLocations(){
    return locations;
  }
}
