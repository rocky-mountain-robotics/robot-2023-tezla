// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.camera;

import java.util.ArrayList;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import edu.wpi.first.apriltag.AprilTagDetection;
import edu.wpi.first.apriltag.AprilTagDetector;
import edu.wpi.first.apriltag.AprilTagPoseEstimator;
import edu.wpi.first.apriltag.AprilTagPoseEstimator.Config;
import edu.wpi.first.math.geometry.Transform3d;
import edu.wpi.first.math.util.Units;
import frc.robot.Constants;

/** Add your docs here. */
public class AprilTagProcessing {
  AprilTagDetector detectorEngine;
  AprilTagPoseEstimator locatorEngine;

  public AprilTagProcessing() {
    detectorEngine = new AprilTagDetector();
    detectorEngine.addFamily("tag16h5");

    var config = detectorEngine.getConfig();
    config.quadSigma = 0.8f;
    detectorEngine.setConfig(config);

    var quadParams = detectorEngine.getQuadThresholdParameters();
    quadParams.minClusterPixels = 250;
    detectorEngine.setQuadThresholdParameters(quadParams);

    locatorEngine = new AprilTagPoseEstimator(new Config(Units.inchesToMeters(6), Constants.CameraConstants.kFX, Constants.CameraConstants.kFY, Constants.CameraConstants.kCX, Constants.CameraConstants.kCY));
  }

  // Detects AprilTags and removes possible false positives
  public AprilTagDetection[] detectAprilTags(Mat image) {
    AprilTagDetection[] detections = new AprilTagDetection[0];

    detections = rawDetectTags(image);
    detections = removeFalsePositives(detections);

    return detections;
  }

  // Detects AprilTags
  public AprilTagDetection[] rawDetectTags(Mat image) {
    Mat greyMat = new Mat();

    Imgproc.cvtColor(image, greyMat, Imgproc.COLOR_BGR2GRAY);

    return detectorEngine.detect(greyMat);
  }

  // Removes false positives from a list of AprilTags
  public AprilTagDetection[] removeFalsePositives(AprilTagDetection[] aprilTags) {
    ArrayList<AprilTagDetection> truePositives = new ArrayList<AprilTagDetection>();

    for(AprilTagDetection tag : aprilTags) {

      // Conditions for false positives (true positves will be added to truePosities arraylist)
      if(!(tag.getHamming() > 0)) {
        truePositives.add(tag);
      }

    }

    // Turns the arraylist into an array, then returns it
    AprilTagDetection[] truePositiveArray = new AprilTagDetection[truePositives.size()];
    truePositiveArray = truePositives.toArray(truePositiveArray);
    return truePositiveArray;
  }

  public Transform3d[] getTagLocations(AprilTagDetection[] detections) {
    ArrayList<Transform3d> locations = new ArrayList<Transform3d>();

    for(AprilTagDetection detection : detections) {
      locations.add(getTagLocation(detection));
    }

    // Turns the arraylist into an array, then returns it
    Transform3d[] locationsArray = new Transform3d[locations.size()];
    locationsArray = locations.toArray(locationsArray);
    return locationsArray;
  }

  public Transform3d getTagLocation(AprilTagDetection detection) {
    return locatorEngine.estimate(detection);
  }

  public Transform3d[] processAprilTags(Mat image) {
    return getTagLocations(detectAprilTags(image));
  }

  public Transform3d[] processAprilTags(AprilTagDetection[] detections) {
    return getTagLocations(detections);
  }
}
