// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.camera;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import edu.wpi.first.apriltag.AprilTagDetection;
import edu.wpi.first.apriltag.AprilTagDetector;
import edu.wpi.first.apriltag.AprilTagPoseEstimator;
import edu.wpi.first.apriltag.AprilTagDetector.QuadThresholdParameters;
import edu.wpi.first.apriltag.AprilTagPoseEstimator.Config;
import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.cscore.CvSink;
import edu.wpi.first.cscore.CvSource;
import edu.wpi.first.cscore.VideoSource;
import edu.wpi.first.math.geometry.Transform3d;
import edu.wpi.first.wpilibj.DriverStation;
import frc.robot.Constants.CameraConstants;
import frc.robot.networktables.CameraDataPublisher;

/** Main thread for detecting and computing position estimates for april tags */
public class AprilTagThread extends Thread {

  // each camera needs a cvSink
  private CvSink m_cvSink;

  /** mat for getting the raw images */
  private Mat mat;
  /** mat to store the grey scale images */
  private Mat greyMat;

  private AprilTagDetector m_detector;
  private AprilTagDetection m_detection;

  private AprilTagPoseEstimator m_poseEstimator;
  private Transform3d m_positionEstimation;

  // network tables
  private CameraDataPublisher table = new CameraDataPublisher();

  // debug
  private boolean debug = true;
  private CvSource m_debugSource;

  /**
   * Creates a new thread for detecting april tags with one camera.
   * Thread is a daemon thread by default.
   * 
   * @param camera the camera to get the video source from
   */
  public AprilTagThread(VideoSource camera) {
    // makes the thread run in the background
    setDaemon(true);

    m_cvSink = new CvSink("Detector Camera 1");
    m_cvSink.setSource(camera);

    m_detector = new AprilTagDetector();

    edu.wpi.first.apriltag.AprilTagDetector.Config config = m_detector.getConfig();
    config.quadSigma = 0.8f;
    m_detector.setConfig(config);

    QuadThresholdParameters quadParams = m_detector.getQuadThresholdParameters();
    quadParams.minClusterPixels = 250;
    m_detector.setQuadThresholdParameters(quadParams);

    // adds the 16h5 family with 0 bit correcting
    m_detector.addFamily("tag16h5", 0);

    m_poseEstimator = new AprilTagPoseEstimator(
      new Config(CameraConstants.kAprilTagSize, CameraConstants.kFX, CameraConstants.kFY, CameraConstants.kCX, CameraConstants.kCY));

    mat = new Mat();
    greyMat = new Mat();

    if (debug) {
      m_debugSource = CameraServer.putVideo("April Tag Debug", 640, 480);
    }
  }

  @Override
  public void run() {

    while(!interrupted()) {
      // mat is in BGR
      long frameTime = m_cvSink.grabFrame(mat);

      // check if a new frame is available
      if (frameTime == 0) {
        // was not able to grab a frame
        DriverStation.reportWarning(m_cvSink.getError(), false);
      }
      else {
        // was able to grab a frame
        Imgproc.cvtColor(mat, greyMat, Imgproc.COLOR_BGR2GRAY);

        AprilTagDetection[] detections = m_detector.detect(greyMat);

        Transform3d[] positionEstimates = new Transform3d[detections.length];

        int i = 0;
        for (AprilTagDetection detection : detections) {
          positionEstimates[i] = m_poseEstimator.estimate(detection);
          i++;
        }

        if (detections.length > 0) {
          synchronized(this) {
            m_detection = detections[0];
            m_positionEstimation = positionEstimates[0];
          }

          table.publish(m_positionEstimation, frameTime);
        }

        if (debug) {
          debugAprilTag(greyMat, detections);
        }
      }
    }
  }

  public synchronized AprilTagDetection getDetection() {
    return m_detection;
  }

  public synchronized Transform3d getPoseEstimation() {
    return m_positionEstimation;
  }

  private void debugAprilTag(Mat picture, AprilTagDetection[] detections) {
    for (AprilTagDetection detection : detections) {
      for (int i = 0; i < 4; i++) {
        Imgproc.line(picture, 
          new Point(detection.getCornerX(i), detection.getCornerY(i)), 
          new Point(detection.getCornerX((i + 1) % 4), detection.getCornerY((i + 1) % 4)),
          new Scalar(0, 255, 0),
          5);
      }

      // Shows the ID for the AprilTag in the center of the tag, in blue
      Imgproc.putText(picture,
        String.valueOf(detection.getId()),
        new Point(detection.getCenterX(), detection.getCenterY()),
        5,
        3,
        new Scalar(0, 0, 255), 5);
    }
    
    m_debugSource.putFrame(picture);
  }
}
