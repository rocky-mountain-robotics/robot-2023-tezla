// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.camera;

import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.cscore.UsbCamera;

/** Add your docs here. */
public class AprilTagCamera {

  private UsbCamera camera;

  private AprilTagThread visionThread;

  public AprilTagCamera() {
    // Get the UsbCamera from CameraServer
    camera = CameraServer.startAutomaticCapture(); 

    camera.setResolution(640, 480);

    // start detecting april tags
    visionThread = new AprilTagThread(camera);
    visionThread.start();
  }

  
}
