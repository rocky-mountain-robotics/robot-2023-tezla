// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

/*
 * Editors:
 * Nathan George
 * Caleb Borlin
 * Andrew George
 */

package frc.robot;

import frc.robot.Constants.StickOfJoy;
import frc.robot.Constants.OperatorConstants;
import frc.robot.camera.AprilTagCamera;
import frc.robot.commands.*;
import frc.robot.components.*;
import frc.robot.subsystems.*;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.Commands;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.button.CommandJoystick;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import edu.wpi.first.wpilibj2.command.button.Trigger;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and trigger mappings) should be declared here.
 */
public class RobotContainer {

  private SendableChooser<CommandBase> autoChooser = new SendableChooser<CommandBase>();

  // The robot's components are defined here..
  private Gyroscope robotGyro = new Gyroscope();
  private CompressorSwitch compressorSwitch = new CompressorSwitch();
  private PressureDetector pressureDetector = new PressureDetector();

  private AprilTagCamera camera = new AprilTagCamera();
  
  // The robot's subsystems and commands are defined here...
  private DriveTrainSubsystem robotDriveTrain = new DriveTrainSubsystem(robotGyro);
  private CompressorSubsystem robotCompressor = new CompressorSubsystem();
  private IntakeSubsystem robotIntake = new IntakeSubsystem(pressureDetector);
  private GripperSubsystem robotGripper = new GripperSubsystem();
  private ShoulderSubsystem robotShoulder = new ShoulderSubsystem(pressureDetector);
  private ArmSubsystem robotArm = new ArmSubsystem(robotShoulder);

  // Commands
  private TeleOpDriveCommand driveCommand = new TeleOpDriveCommand(robotDriveTrain);
  private AutoBalanceCommand autoBalanceCommand = new AutoBalanceCommand(robotDriveTrain, robotGyro);

  private ToggleGripperCommand toggleGripperCommand = new ToggleGripperCommand(robotGripper);

  private DeployIntakeCommand deployIntakeCommand = new DeployIntakeCommand(robotIntake);
  private RetractIntakeCommand retractIntakeCommand = new RetractIntakeCommand(robotIntake);
  private BlackHolenateCommand blackHolenateCommand = new BlackHolenateCommand(robotIntake);
  private WhiteHolenateCommand whiteHolenateCommand = new WhiteHolenateCommand(robotIntake);

  private TeleOpArmElbowCommand teleOpArmElbowCommand = new TeleOpArmElbowCommand(robotArm);

  private DeployShoulderCommand armShoulderOutCommand = new DeployShoulderCommand(robotShoulder);
  private RetractShoulderCommand armShoulderInCommand = new RetractShoulderCommand(robotShoulder);

  private DeployManipCommandGroup deployManipCommandGroup = new DeployManipCommandGroup(robotIntake, robotArm, robotShoulder);
  private RetractManipCommandGroup retractManipCommandGroup = new RetractManipCommandGroup(robotIntake, robotArm, robotShoulder);

  // Replace with CommandXboxController or CommandJoystick if needed
  private final CommandJoystick driveController =
      new CommandJoystick(OperatorConstants.kDriverControllerPort);
  private final CommandJoystick manipulatorController = 
    new CommandJoystick(OperatorConstants.kManipulatorControllerPort);

  /** The container for the robot. Contains subsystems, OI devices, and commands. */
  public RobotContainer() {
    // Configure the trigger bindings
    configureBindings();

    configureAutoOptions();

    // default commands
    robotDriveTrain.setDefaultCommand(driveCommand);
    robotArm.setDefaultCommand(teleOpArmElbowCommand);

    if (compressorSwitch.isOn()) {
      robotCompressor.enable();
    } else {
      robotCompressor.disable();
    }
  }

  /**
   * Use this method to define your trigger->command mappings. Triggers can be created via the
   * {@link Trigger#Trigger(java.util.function.BooleanSupplier)} constructor with an arbitrary
   * predicate, or via the named factories in {@link
   * edu.wpi.first.wpilibj2.command.button.CommandGenericHID}'s subclasses for {@link
   * CommandXboxController Xbox}/{@link edu.wpi.first.wpilibj2.command.button.CommandPS4Controller
   * PS4} controllers or {@link edu.wpi.first.wpilibj2.command.button.CommandJoystick Flight
   * joysticks}.
   */
  private void configureBindings() {

    new Trigger(compressorSwitch::isOn)
      .onTrue(robotCompressor.getEnableCommand())
      .onFalse(robotCompressor.getDisableCommand());

    driveController.button(StickOfJoy.kBaseButton9)
      .onTrue(Commands.runOnce(robotDriveTrain::setBreakMode, robotDriveTrain))
      .onFalse(Commands.runOnce(robotDriveTrain::setCoastMode, robotDriveTrain));
    
    driveController.button(StickOfJoy.kTriggerButton)
      .onTrue(Commands.runOnce(robotDriveTrain::setHighGear, robotDriveTrain))
      .onFalse(Commands.runOnce(robotDriveTrain::setLowGear, robotDriveTrain));
    
    driveController.button(StickOfJoy.kTopButton6).onTrue(deployIntakeCommand);
    driveController.button(StickOfJoy.kTopButton4).onTrue(retractIntakeCommand);
    driveController.button(StickOfJoy.kBaseButton12).whileTrue(whiteHolenateCommand);
    driveController.button(StickOfJoy.kThumbButton)
      .whileTrue(blackHolenateCommand)
      .onFalse(new WhiteHolenateCommand(robotIntake).withTimeout(0.1)
        .andThen(new BlackHolenateCommand(robotIntake).withTimeout(0.2)));
    
    driveController.button(StickOfJoy.kBaseButton11)
      .whileTrue(autoBalanceCommand);

    manipulatorController.button(StickOfJoy.kTopButton3)
      .toggleOnTrue(toggleGripperCommand);
      
    manipulatorController.button(StickOfJoy.kTriggerButton)
      .whileTrue(teleOpArmElbowCommand);

    manipulatorController.button(StickOfJoy.kTopButton6)
      .onTrue(armShoulderOutCommand.deadlineWith(new ArmHoldCommand(robotArm)));
    manipulatorController.button(StickOfJoy.kTopButton4)
      .onTrue(armShoulderInCommand.deadlineWith(new ArmHoldCommand(robotArm)));
      
    manipulatorController.button(StickOfJoy.kBaseButton10)
      //.and(pressureDetector :: atPressure)
      .onTrue(deployManipCommandGroup);
    manipulatorController.button(StickOfJoy.kBaseButton9)
    //    .and(pressureDetector :: atPressure)  
      .onTrue(retractManipCommandGroup);
  }

  public void configureAutoOptions() {
    autoChooser.setDefaultOption("Do Nothing Auto", new InstantCommand());
    autoChooser.addOption("Auto Balance Auto", Autos.balanceAuto(robotDriveTrain, robotIntake, robotGripper, robotGyro));
    autoChooser.addOption("Score Cone Auto", Autos.scoreConeAuto(robotDriveTrain, robotArm, robotShoulder, robotIntake, robotGripper));

    SmartDashboard.putData("Auto Chooser", autoChooser);
  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    // An example command will be run in autonomous
    return autoChooser.getSelected();
  }

  /**
   * call when disabled
   */
  public void disable() {
    // set coast mode to get off of charge station
    robotDriveTrain.setCoastMode();
  }
}
