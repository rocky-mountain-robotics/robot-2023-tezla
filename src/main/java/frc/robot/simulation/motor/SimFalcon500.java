// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.simulation.motor;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;

import edu.wpi.first.hal.SimDouble;
import edu.wpi.first.hal.SimInt;
import edu.wpi.first.wpilibj.simulation.SimDeviceSim;

/** Simulates a Falcon 500 and its integrated encoder */
public class SimFalcon500 {
  private SimDouble appliedOutput;

  private SimInt m_simEncoderVelocity;
  private SimInt m_simEncoderPosition;

  public SimFalcon500(WPI_TalonFX motorController) {
    String name = "Talon FX[" + motorController.getDeviceID() + "]";
    SimDeviceSim motorWrapper = new SimDeviceSim("CANMotor:" + name);
    SimDeviceSim encoderWrapper = new SimDeviceSim("CANEncoder:" + name + "/Integrated Sensor");

    if (motorWrapper != null) {
      appliedOutput = motorWrapper.getDouble("motorOutputLeadVoltage");

      m_simEncoderVelocity = encoderWrapper.getInt("velocity");
      m_simEncoderPosition = encoderWrapper.getInt("rawPositionInput");
    }
  }

  /**
   * gets the voltage applied to the motor
   * 
   * @return the voltage
   */
  public double getVoltage() {
    return appliedOutput.get();
  }


  public void update(double velocity, double period) {
    m_simEncoderVelocity.set((int) (velocity * (2048) / (60 * 1000/100)));
    m_simEncoderPosition.set(m_simEncoderPosition.get() + (int) (velocity * (period / 60) * (2048)));
  }
}
