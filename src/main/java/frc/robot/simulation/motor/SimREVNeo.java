// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.simulation.motor;

import com.revrobotics.CANSparkMax;

import edu.wpi.first.hal.SimDouble;
import edu.wpi.first.wpilibj.simulation.SimDeviceSim;

/** Simulates a REV Neo and its integrated encoder */
public class SimREVNeo {

  private SimDouble appliedOutput;

  private SimDouble m_simEncoderVelocity;
  private SimDouble m_simEncoderPosition;

  /**
   * Creates a Simulated REV Neo and its integrated encoder
   * 
   * @param motorController Spark Max the motor is connected to
   */
  public SimREVNeo(CANSparkMax motorController) {
    SimDeviceSim deviceWrapper = new SimDeviceSim("SPARK MAX ", motorController.getDeviceId());

    if (deviceWrapper != null) {
      appliedOutput = deviceWrapper.getDouble("Applied Output");

      m_simEncoderVelocity = deviceWrapper.getDouble("Velocity Pre-Conversion");
      m_simEncoderPosition = deviceWrapper.getDouble("Position");
    }
  }

  /**
   * gets the voltage applied to the motor
   * 
   * @return the voltage
   */
  public double getVoltage() {
    return appliedOutput.get();
  }

  public void update(double velocity, double period) {
    period = period / 60;

    m_simEncoderVelocity.set(velocity);
    m_simEncoderPosition.set(m_simEncoderPosition.get() + velocity * period);
  }

  public void setPosition(double positionRotations) {
    m_simEncoderPosition.set(positionRotations);
  }
}
