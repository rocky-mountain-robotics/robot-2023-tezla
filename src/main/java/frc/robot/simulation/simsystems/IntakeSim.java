// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.simulation.simsystems;

import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.simulation.DoubleSolenoidSim;
import frc.robot.Constants.CAN;
import frc.robot.Constants.SolenoidChannel;

/** Add your docs here. */
public class IntakeSim {

  private DoubleSolenoidSim intakeSolenoidSim;

  public IntakeSim() {
    intakeSolenoidSim = new DoubleSolenoidSim(CAN.kPneumaticHub, PneumaticsModuleType.REVPH, SolenoidChannel.kIntakeForward, SolenoidChannel.kIntakeReverse);
    intakeSolenoidSim.set(Value.kReverse);
  }

  public void setIntakeSolenoidForward() {
    intakeSolenoidSim.set(Value.kForward);
  }

  public void setIntakeSolenoidReverse() {
    intakeSolenoidSim.set(Value.kReverse);
  }

  public Value getIntakeSolenoid() {
    return intakeSolenoidSim.get();
  }
}
