// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.simulation.simsystems;

import com.revrobotics.CANSparkMax;

import edu.wpi.first.math.system.plant.DCMotor;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.simulation.DifferentialDrivetrainSim;
import edu.wpi.first.wpilibj.simulation.DoubleSolenoidSim;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Constants.CAN;
import frc.robot.Constants.SolenoidChannel;
import frc.robot.simulation.motor.SimREVNeo;
import frc.robot.wpilibj.ADIS16470_IMUSim;

import static frc.robot.Constants.DriveConstants.*;

/** Simulation for the drivetrain subsystem */
public class DriveTrainSim {
  private SimREVNeo leftMotorSim;
  private SimREVNeo leftMotorFollowerSim;
  private SimREVNeo rightMotorSim;
  private SimREVNeo rightMotorFollowerSim;

  private DCMotor gearboxMotors;

  private DifferentialDrivetrainSim driveSim;

  private DoubleSolenoidSim shifterSim;

  private ADIS16470_IMUSim gyroSim;

  private double gearRatio;
  
  private double lastUpdateTime;

  private Field2d field2d;

  public DriveTrainSim(
      CANSparkMax leftMotor,
      CANSparkMax leftMotorFollower,
      CANSparkMax rightMotor,
      CANSparkMax rightMotorFollower,
      ADIS16470_IMUSim gyro) {

    leftMotorSim = new SimREVNeo(leftMotor);
    leftMotorFollowerSim = new SimREVNeo(leftMotorFollower);
    rightMotorSim = new SimREVNeo(rightMotor);
    rightMotorFollowerSim = new SimREVNeo(rightMotorFollower);

    gearboxMotors = DCMotor.getNEO(2);

    driveSim = new DifferentialDrivetrainSim(
      gearboxMotors, 
      kHighGearRatio, 
      10, 
      70, 
      kWheelDiameter / 2,
      kTrackWidth, 
      null);

    gearRatio = kHighGearRatio;

    shifterSim = new DoubleSolenoidSim(CAN.kPneumaticHub, PneumaticsModuleType.REVPH, SolenoidChannel.kDriveShifterHighGear, SolenoidChannel.kDriveShifterLowGear);
    setShifterReverse();

    gyroSim = gyro;

    lastUpdateTime = Timer.getFPGATimestamp();

    field2d = new Field2d();
    SmartDashboard.putData(field2d);
  }

  public void update() {
    double period = Timer.getFPGATimestamp() - lastUpdateTime;
    lastUpdateTime += period;

    driveSim.setInputs(leftMotorSim.getVoltage(), rightMotorSim.getVoltage());
    driveSim.update(period);

    double leftVelocityRPM = Units.radiansPerSecondToRotationsPerMinute(
      driveSim.getLeftVelocityMetersPerSecond() / (kWheelDiameter / 2)) * gearRatio;
    double rightVelocityRPM = Units.radiansPerSecondToRotationsPerMinute(
        driveSim.getRightVelocityMetersPerSecond() / (kWheelDiameter / 2)) * gearRatio;

    leftMotorSim.update(leftVelocityRPM, period);
    leftMotorFollowerSim.update(leftVelocityRPM, period);
    rightMotorSim.update(rightVelocityRPM, period);
    rightMotorFollowerSim.update(rightVelocityRPM, period);

    field2d.getObject("sim").setPose(driveSim.getPose());

    gyroSim.setGyroAngleZ(driveSim.getHeading().getDegrees());
  }

  public void setShifterForward() {
    shifterSim.set(Value.kForward);

    driveSim.setCurrentGearing(kHighGearRatio);
    gearRatio = kHighGearRatio;
  }

  public void setShifterReverse() {
    shifterSim.set(Value.kReverse);

    driveSim.setCurrentGearing(kLowGearRatio);
    gearRatio = kLowGearRatio;
  }

  public Value getShifter() {
    return shifterSim.get();
  }
}
