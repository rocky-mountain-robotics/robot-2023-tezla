// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.simulation.simsystems;

import com.revrobotics.CANSparkMax;

import edu.wpi.first.math.Matrix;
import edu.wpi.first.math.Nat;
import edu.wpi.first.math.system.plant.DCMotor;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.simulation.DoubleSolenoidSim;
import edu.wpi.first.wpilibj.simulation.SingleJointedArmSim;
import edu.wpi.first.wpilibj.smartdashboard.Mechanism2d;
import edu.wpi.first.wpilibj.smartdashboard.MechanismLigament2d;
import edu.wpi.first.wpilibj.smartdashboard.MechanismRoot2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Constants.CAN;
import frc.robot.Constants.SolenoidChannel;
import frc.robot.simulation.motor.SimREVNeo;

import static frc.robot.Constants.ArmConstants.*;

/** Add your docs here. */
public class ArmSim {

  private SimREVNeo motorSim;

  private DCMotor elbowGearboxMotor;

  private SingleJointedArmSim lowerArmSim;

  private DoubleSolenoidSim shoulderSolenoidSim;
  
  private double lastUpdateTime;

  // Visualizing Sim Arm
  private Mechanism2d armMechanism;
  private MechanismRoot2d armRoot;
  private MechanismLigament2d upperArm;
  private MechanismLigament2d lowerArm;

  public ArmSim(CANSparkMax elbowMotor) {
    motorSim = new SimREVNeo(elbowMotor);

    elbowGearboxMotor = DCMotor.getNEO(1);

    lowerArmSim = new SingleJointedArmSim(
      elbowGearboxMotor, 
      kGearRatio, 
      SingleJointedArmSim.estimateMOI(kLowerArmLength, kLowerArmMass), 
      kLowerArmLength, 
      -2 * Math.PI, 2 * Math.PI, 
      kLowerArmMass, 
      true);
    
    lowerArmSim.setState(Matrix.mat(Nat.N2(), Nat.N1()).fill(Units.degreesToRadians(kArmStartingAngle), 0));

    shoulderSolenoidSim = new DoubleSolenoidSim(CAN.kPneumaticHub, PneumaticsModuleType.REVPH, SolenoidChannel.kArmPivotForward, SolenoidChannel.kArmPivotReverse);
    shoulderSolenoidSim.set(Value.kReverse);
    
    lastUpdateTime = Timer.getFPGATimestamp();
    
    // construct mechanism
    armMechanism = new Mechanism2d(1.7, 1.2);

    armRoot = armMechanism.getRoot("arm", 0.3, 0);
    upperArm = armRoot.append(new MechanismLigament2d(
      "upper arm", 
      kUpperArmLength, 
      kUpperArmInAngle));
    
    lowerArm = upperArm.append(new MechanismLigament2d(
      "lower arm", 
      kLowerArmLength, 
      kArmStartingAngle - upperArm.getAngle()));

    SmartDashboard.putData("Arm", armMechanism);
  }

  public void update() {
    double period = Timer.getFPGATimestamp() - lastUpdateTime;
    lastUpdateTime += period;

    lowerArmSim.setInputVoltage(motorSim.getVoltage());
    lowerArmSim.update(period);

    motorSim.update(Units.radiansPerSecondToRotationsPerMinute(lowerArmSim.getVelocityRadPerSec()) * kGearRatio, period);

    // Update mechanism
    lowerArm.setAngle(Units.radiansToDegrees(lowerArmSim.getAngleRads()) - upperArm.getAngle());
  }

  public void setShoulderSolenoidForward() {
    shoulderSolenoidSim.set(Value.kForward);

    
    motorSim.setPosition(
      Units.degreesToRotations(
        (Units.radiansToDegrees(lowerArmSim.getAngleRads()) - kUpperArmOutAngle)
      ) * kGearRatio
    );

    // update the displayed arm
    upperArm.setAngle(kUpperArmOutAngle);
    lowerArm.setAngle(Units.radiansToDegrees(lowerArmSim.getAngleRads()) - upperArm.getAngle());
  }

  public void setShoulderSolenoidReverse() {
    shoulderSolenoidSim.set(Value.kReverse);

    motorSim.setPosition(
      Units.degreesToRotations(
        (Units.radiansToDegrees(lowerArmSim.getAngleRads()) - kUpperArmInAngle)
      ) * kGearRatio
    );

    // update the displayed arm
    upperArm.setAngle(kUpperArmInAngle);
    lowerArm.setAngle(Units.radiansToDegrees(lowerArmSim.getAngleRads()) - upperArm.getAngle());
  }

  public Value getShoulderSolenoid() {
    return shoulderSolenoidSim.get();
  }
}
